<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ApottiAttachments Controller
 *
 * @property \App\Model\Table\ApottiAttachmentsTable $ApottiAttachments
 *
 * @method \App\Model\Entity\ApottiAttachment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ApottiAttachmentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Apotti'],
        ];
        $apottiAttachments = $this->paginate($this->ApottiAttachments);

        $this->set(compact('apottiAttachments'));
    }

    /**
     * View method
     *
     * @param string|null $id Apotti Attachment id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $apottiAttachment = $this->ApottiAttachments->get($id, [
            'contain' => ['Apotti'],
        ]);

        $this->set('apottiAttachment', $apottiAttachment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $apottiAttachment = $this->ApottiAttachments->newEmptyEntity();
        if ($this->request->is('post')) {
            $apottiAttachment = $this->ApottiAttachments->patchEntity($apottiAttachment, $this->request->getData());
            if ($this->ApottiAttachments->save($apottiAttachment)) {
                $this->Flash->success(__('The apotti attachment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The apotti attachment could not be saved. Please, try again.'));
        }
        $apottis = $this->ApottiAttachments->Apottis->find('list', ['limit' => 200]);
        $this->set(compact('apottiAttachment', 'apottis'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Apotti Attachment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $apottiAttachment = $this->ApottiAttachments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $apottiAttachment = $this->ApottiAttachments->patchEntity($apottiAttachment, $this->request->getData());
            if ($this->ApottiAttachments->save($apottiAttachment)) {
                $this->Flash->success(__('The apotti attachment has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The apotti attachment could not be saved. Please, try again.'));
        }
        $apottis = $this->ApottiAttachments->Apotti->find('list', ['limit' => 200]);
        $this->set(compact('apottiAttachment', 'apottis'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Apotti Attachment id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $apottiAttachment = $this->ApottiAttachments->get($id);
        if ($this->ApottiAttachments->delete($apottiAttachment)) {
            $this->Flash->success(__('The apotti attachment has been deleted.'));
        } else {
            $this->Flash->error(__('The apotti attachment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
