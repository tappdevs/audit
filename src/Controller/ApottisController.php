<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Apottis Controller
 *
 * @property \App\Model\Table\ApottisTable $Apottis
 *
 * @method \App\Model\Entity\Apotti[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ApottisController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $apottis = $this->paginate($this->Apottis);

        $this->set(compact('apottis'));
    }

    /**
     * View method
     *
     * @param string|null $id Apotti id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $apotti = $this->Apottis->get($id, [
            'contain' => [],
        ]);

        $this->set('apotti', $apotti);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $apotti = $this->Apottis->newEmptyEntity();
        if ($this->request->is('post')) {
            $apotti = $this->Apottis->patchEntity($apotti, $this->request->getData());
            if ($this->Apottis->save($apotti)) {
                $this->Flash->success(__('The apotti has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The apotti could not be saved. Please, try again.'));
        }
        $this->set(compact('apotti'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Apotti id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $apotti = $this->Apottis->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $apotti = $this->Apottis->patchEntity($apotti, $this->request->getData());
            if ($this->Apottis->save($apotti)) {
                $this->Flash->success(__('The apotti has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The apotti could not be saved. Please, try again.'));
        }
        $this->set(compact('apotti'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Apotti id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $apotti = $this->Apottis->get($id);
        if ($this->Apottis->delete($apotti)) {
            $this->Flash->success(__('The apotti has been deleted.'));
        } else {
            $this->Flash->error(__('The apotti could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
