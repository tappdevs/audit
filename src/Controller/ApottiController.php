<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;

/**
 * Apotti Controller
 *
 * @property \App\Model\Table\ApottiTable $Apotti
 *
 * @method \App\Model\Entity\Apotti[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ApottiController extends AppController
{

    /*public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'finder' => 'auth'
                ]
            ],
        ]);
    }*/

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public $apottiType = [
        'জালিয়াতী' => 'জালিয়াতী',
        'আর্থিক ক্ষতি' => 'আর্থিক ক্ষতি',
        'আর্থিক বিধির ব্যত্যয়' => 'আর্থিক বিধির ব্যত্যয়',
        'নিরীক্ষাকালীন অসহযোগীতা' => 'নিরীক্ষাকালীন অসহযোগীতা',
        'অন্যান্য' => 'অন্যান্য',
    ];


    public function index()
    {
        $apotties = $this->paginate($this->Apotti);

        $apotti_kari_organization = $this->Apotti->find()
            ->combine('apotti_kari_organization', 'apotti_kari_organization')
            ->toArray();
        $apotti_type = $this->apottiType;
        $apotti_ministry = $this->Apotti->find()
            ->combine('apotti_ministry', 'apotti_ministry')
            ->toArray();


        if ($this->request->is('post')) {
            $getData = $this->request->getData();


            $apotties = $this->Apotti->find();

            if (!empty($getData['apotti_no'])) {
                $apotties = $apotties->where(['apotti_no' => $getData['apotti_no']]);
            }
            if (!empty($getData['apotti_kari_organization'])) {
                $apotties = $apotties->where(['apotti_kari_organization' => $getData['apotti_kari_organization']]);
            }
            if (!empty($getData['apotti_type'])) {
                $apotties = $apotties->where(['apotti_type' => $getData['apotti_type']]);
            }
            if (!empty($getData['apotti_ministry'])) {
                $apotties = $apotties->where(['apotti_ministry' => $getData['apotti_ministry']]);
            }
            if (!empty($getData['apotti_year'])) {
                $apotties = $apotties->where(['apotti_year' => $getData['apotti_year']]);
            }
            /*if (!empty($getData['apotti_date'])) {
                $getDate = explode(' - ', $getData['apotti_date']);
                $startDate = date('Y-m-d', strtotime($getDate[0]));
                $endDate = date('Y-m-d', strtotime($getDate[1]));

                $apotties = $apotties->where(function (QueryExpression $exp, Query $q) use ($startDate, $endDate) {
                    return $exp->between('DATE(apotti_date)', $startDate, $endDate);
                });
            }*/

            $apotties = $this->paginate($apotties);

        }


        $this->set(compact('apotti_type', 'apotti_kari_organization', 'apotti_ministry'));
        $this->set(compact('apotties'));
        $this->set('cakeTitle', 'আপত্তি লিস্ট ও ফিল্টার');

        $param = $this->request->getParam('controller') . '/' . $this->request->getParam('action');
        $this->set('param', $param);
    }


    public function nishpottikrito()
    {

//        $ApottiAttachments = TableRegistry::getTableLocator()->get('Apotti');
//        $apottiAttachmentsMain = $ApottiAttachments->find()->where(['apotti_id' => $id, 'attachment_type' => 'main']);


        $apotties = $this->paginate($this->Apotti);

        $apotti_kari_organization = $this->Apotti->find()
            ->combine('apotti_kari_organization', 'apotti_kari_organization')
            ->toArray();
        $apotti_type = $this->apottiType;
        $apotti_ministry = $this->Apotti->find()
            ->combine('apotti_ministry', 'apotti_ministry')
            ->toArray();


        if ($this->request->is('post')) {
            $getData = $this->request->getData();


            $apotties = $this->Apotti->find();

            if (!empty($getData['apotti_no'])) {
                $apotties = $apotties->where(['apotti_no' => $getData['apotti_no']]);
            }
            if (!empty($getData['apotti_kari_organization'])) {
                $apotties = $apotties->where(['apotti_kari_organization' => $getData['apotti_kari_organization']]);
            }
            if (!empty($getData['apotti_type'])) {
                $apotties = $apotties->where(['apotti_type' => $getData['apotti_type']]);
            }
            if (!empty($getData['apotti_ministry'])) {
                $apotties = $apotties->where(['apotti_ministry' => $getData['apotti_ministry']]);
            }
            if (!empty($getData['apotti_year'])) {
                $apotties = $apotties->where(['apotti_year' => $getData['apotti_year']]);
            }
            /*if (!empty($getData['apotti_date'])) {
                $getDate = explode(' - ', $getData['apotti_date']);
                $startDate = date('Y-m-d', strtotime($getDate[0]));
                $endDate = date('Y-m-d', strtotime($getDate[1]));

                $apotties = $apotties->where(function (QueryExpression $exp, Query $q) use ($startDate, $endDate) {
                    return $exp->between('DATE(apotti_date)', $startDate, $endDate);
                });
            }*/

            $apotties = $this->paginate($apotties);

        }


        $this->set(compact('apotti_type', 'apotti_kari_organization', 'apotti_ministry'));
        $this->set(compact('apotties'));
        $this->set('cakeTitle', 'আপত্তি লিস্ট ও ফিল্টার');

        $param = $this->request->getParam('controller') . '/' . $this->request->getParam('action');
        $this->set('param', $param);
    }

    /**
     * View method
     *
     * @param string|null $id Apotti id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $apotti = $this->Apotti->get($id, [
            'contain' => [],
        ]);

        $ApottiAttachments = TableRegistry::getTableLocator()->get('ApottiAttachments');
        $apottiAttachmentsMain = $ApottiAttachments->find()->where(['apotti_id' => $id, 'attachment_type' => 'main']);

        $attachedFilesMain = [];

        foreach ($apottiAttachmentsMain as $apottiAttachment) {
            $arrTmp = [];
            $arrTmp['id'] = $apottiAttachment['id'];
            $arrTmp['user_define_name'] = $apottiAttachment['user_define_name'];
            $arrTmp['attachment_path'] = $apottiAttachment['attachment_path'] . $apottiAttachment['attachment_name'];
            $attachedFilesMain[] = $arrTmp;
        }

        $apottiAttachmentsOther = $ApottiAttachments->find()->where(['apotti_id' => $id, 'attachment_type' => 'other']);

        $attachedFilesOther = [];

        foreach ($apottiAttachmentsOther as $apottiAttachment) {
            $arrTmp = [];
            $arrTmp['id'] = $apottiAttachment['id'];
            $arrTmp['user_define_name'] = $apottiAttachment['user_define_name'];
            $arrTmp['attachment_path'] = $apottiAttachment['attachment_path'] . $apottiAttachment['attachment_name'];
            $attachedFilesOther[] = $arrTmp;
        }


        $this->set(compact('attachedFilesMain', 'attachedFilesOther'));
        $this->set('apotti', $apotti);
        $this->set('cakeTitle', $apotti->apotti_no);

        $param = $this->request->getParam('controller') . '/' . $this->request->getParam('action');
        $this->set('param', $param);

    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {

        $apotties = $this->paginate($this->Apotti);

        $apottiAttachmentTable = TableRegistry::getTableLocator()->get('ApottiAttachments');

        $this->set('cakeTitle', 'আপত্তি আপলোড করুন');

        $apotties = $this->Apotti->newEmptyEntity();
        if ($this->request->is('post')) {
            $getData = $this->request->getData();

            $getData['apotti_date'] = date('Y-m-d', strtotime($getData['apotti_date']));

            $apotties = $this->Apotti->patchEntity($apotties, $getData);

            if ($this->Apotti->save($apotties)) {
                $attachments = $getData['apotti_attachment'];
                foreach ($attachments as $key => $attachment) {
                    if ($attachment->getClientFilename() != '') {

                        $pathinfo = pathinfo($attachment->getClientFilename());
                        $extension = $pathinfo['extension'];

                        $fileName = $apotties['id'] . '_' . uniqid() . '.' . $extension;

//                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS;
                        /*if (!file_exists($uploadDirectory)) {
                            mkdir($uploadDirectory, 777, true);
                        }*/

                        $targetPath = $uploadDirectory . $fileName;

                        if ($fileName) {
                            $attachment->moveTo($targetPath);

                            $apottiAttachmentEntity = $apottiAttachmentTable->newEmptyEntity();

                            $apottiAttachmentEntity->apotti_id = $apotties['id'];
                            $apottiAttachmentEntity->attachment_type = 'main';//$attachment->getClientMediaType();
                            $apottiAttachmentEntity->user_define_name = $getData['user_define_name'][$key];
                            $apottiAttachmentEntity->attachment_name = $fileName;
//                            $apottiAttachmentEntity->attachment_path = 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                            $apottiAttachmentEntity->attachment_path = 'upload' . DS;


                            $apottiAttachmentTable->save($apottiAttachmentEntity);

                        }
                    }
                }


                $apotti_attachment_other = $getData['apotti_attachment_other'];
                foreach ($apotti_attachment_other as $key => $attachment_other) {
                    if ($attachment_other->getClientFilename() != '') {

                        $pathinfo = pathinfo($attachment_other->getClientFilename());
                        $extension = $pathinfo['extension'];

                        $fileName = $apotties['id'] . '_' . uniqid() . '.' . $extension;

//                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS;
                        /*if (!file_exists($uploadDirectory)) {
                            mkdir($uploadDirectory, 777, true);
                        }*/

                        $targetPath = $uploadDirectory . $fileName;

                        if ($fileName) {
                            $attachment_other->moveTo($targetPath);

                            $apottiAttachmentEntity = $apottiAttachmentTable->newEmptyEntity();

                            $apottiAttachmentEntity->apotti_id = $apotties['id'];
                            $apottiAttachmentEntity->attachment_type = 'other';//$attachment_other->getClientMediaType();
                            $apottiAttachmentEntity->user_define_name = $getData['user_define_name'][$key];
                            $apottiAttachmentEntity->attachment_name = $fileName;
//                            $apottiAttachmentEntity->attachment_path = 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                            $apottiAttachmentEntity->attachment_path = 'upload' . DS;

                            $apottiAttachmentTable->save($apottiAttachmentEntity);
                        }
                    }
                }


                $this->Flash->success(__('The apotti has been saved.'));

                return $getData['btn-save'] == 'save-list' ? $this->redirect(['action' => 'index']) : $this->redirect(['action' => 'edit', $apotties['id']]);

            }
            $this->Flash->error(__('The apotti could not be saved. Please, try again.'));
        }

        $this->set(compact('apotties'));

        $param = $this->request->getParam('controller') . '/' . $this->request->getParam('action');
        $this->set('param', $param);
    }

    /**
     * Edit method
     *
     * @param string|null $id Apotti id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $apotties = $this->Apotti->get($id, [
            'contain' => [],
        ]);
        $apottiAttachmentTable = TableRegistry::getTableLocator()->get('ApottiAttachments');
        $apottiAttachmentData = $apottiAttachmentTable->find()->where(['apotti_id' => $id])->toArray();

//        pr($apottiAttachmentData->toArray());die;

        if ($this->request->is(['patch', 'post', 'put'])) {
            $getData = $this->request->getData();
            $getData['apotti_date'] = date('Y-m-d', strtotime($getData['apotti_date']));

            $apotties = $this->Apotti->patchEntity($apotties, $getData);

            if ($this->Apotti->save($apotties)) {

                $attachments = $getData['apotti_attachment'];

                foreach ($attachments as $key => $attachment) {
                    if ($attachment->getClientFilename() != '') {
                        $pathinfo = pathinfo($attachment->getClientFilename());
                        $extension = $pathinfo['extension'];

                        $fileName = $apotties['id'] . '_' . uniqid() . '.' . $extension;

//                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS;
                        /*if (!file_exists($uploadDirectory)) {
                            mkdir($uploadDirectory, 777, true);
                        }*/

                        $targetPath = $uploadDirectory . $fileName;

                        if ($fileName) {
                            $attachment->moveTo($targetPath);

                            $apottiAttachmentEntity = $apottiAttachmentTable->newEmptyEntity();

                            $apottiAttachmentEntity->apotti_id = $apotties['id'];
                            $apottiAttachmentEntity->attachment_type = 'main';//$attachment->getClientMediaType();
                            $apottiAttachmentEntity->user_define_name = $getData['user_define_name'][$key];
                            $apottiAttachmentEntity->attachment_name = $fileName;
//                            $apottiAttachmentEntity->attachment_path = 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                            $apottiAttachmentEntity->attachment_path = 'upload' . DS;


                            $apottiAttachmentTable->save($apottiAttachmentEntity);

                        }
                    }
                }


                $apotti_attachment_other = $getData['apotti_attachment_other'];
                foreach ($apotti_attachment_other as $key => $attachment_other) {
                    if ($attachment_other->getClientFilename() != '') {

                        $pathinfo = pathinfo($attachment_other->getClientFilename());
                        $extension = $pathinfo['extension'];

                        $fileName = $apotties['id'] . '_' . uniqid() . '.' . $extension;

//                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                        $uploadDirectory = WWW_ROOT . 'img' . DS . 'upload' . DS;
                        /*if (!file_exists($uploadDirectory)) {
                            mkdir($uploadDirectory, 777, true);
                        }*/

                        $targetPath = $uploadDirectory . $fileName;

                        if ($fileName) {
                            $attachment_other->moveTo($targetPath);

                            $apottiAttachmentEntity = $apottiAttachmentTable->newEmptyEntity();

                            $apottiAttachmentEntity->apotti_id = $apotties['id'];
                            $apottiAttachmentEntity->attachment_type = 'other';//$attachment_other->getClientMediaType();
                            $apottiAttachmentEntity->user_define_name = $getData['user_define_name'][$key];
                            $apottiAttachmentEntity->attachment_name = $fileName;
//                            $apottiAttachmentEntity->attachment_path = 'upload' . DS . 'apotti_id_' . $apotties['id'] . DS;
                            $apottiAttachmentEntity->attachment_path = 'upload' . DS;

                            $apottiAttachmentTable->save($apottiAttachmentEntity);
                        }
                    }
                }

                $this->Flash->success(__('The apotti has been saved.'));

                return $getData['btn-save'] == 'save-list' ? $this->redirect(['action' => 'index']) : $this->redirect(['action' => 'edit', $apotties['id']]);

            }
            $this->Flash->error(__('The apotti could not be saved. Please, try again.'));
        }


        $ApottiAttachments = TableRegistry::getTableLocator()->get('ApottiAttachments');
        $apottiAttachmentsMain = $ApottiAttachments->find()->where(['apotti_id' => $id, 'attachment_type' => 'main']);

        $attachedFilesMain = [];

        foreach ($apottiAttachmentsMain as $apottiAttachment) {
            $arrTmp = [];
            $arrTmp['id'] = $apottiAttachment['id'];
            $arrTmp['user_define_name'] = $apottiAttachment['user_define_name'];
            $arrTmp['attachment_path'] = $apottiAttachment['attachment_path'] . $apottiAttachment['attachment_name'];
            $attachedFilesMain[] = $arrTmp;
        }

        $apottiAttachmentsOther = $ApottiAttachments->find()->where(['apotti_id' => $id, 'attachment_type' => 'other']);

        $attachedFilesOther = [];

        foreach ($apottiAttachmentsOther as $apottiAttachment) {
            $arrTmp = [];
            $arrTmp['id'] = $apottiAttachment['id'];
            $arrTmp['user_define_name'] = $apottiAttachment['user_define_name'];
            $arrTmp['attachment_path'] = $apottiAttachment['attachment_path'] . $apottiAttachment['attachment_name'];
            $attachedFilesOther[] = $arrTmp;
        }


        $this->set(compact('attachedFilesMain', 'attachedFilesOther'));
        $this->set('cakeTitle', 'Edit ' . $apotties->apotti_no);
        $this->set(compact('apotties'));
        $this->set(compact('apottiAttachmentData'));

        $param = $this->request->getParam('controller') . '/' . $this->request->getParam('action');
        $this->set('param', $param);
    }

    /**
     * Delete method
     *
     * @param string|null $id Apotti id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {


        $this->request->allowMethod(['post', 'delete']);
        $apotti = $this->Apotti->get($id);

        if ($this->Apotti->delete($apotti)) {
            $this->Flash->success(__('The apotti has been deleted.'));
        } else {
            $this->Flash->error(__('The apotti could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Apotti', 'action' => 'index']);
    }

    public function officeConfiguration()
    {

        $param = $this->request->getParam('controller') . '/' . $this->request->getParam('action');
        $this->set('cakeTitle', 'অফিস কনফিগারেশন');
        $this->set('param', $param);
    }
}
