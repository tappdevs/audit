<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Apottis Model
 *
 * @method \App\Model\Entity\Apotti newEmptyEntity()
 * @method \App\Model\Entity\Apotti newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Apotti[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Apotti get($primaryKey, $options = [])
 * @method \App\Model\Entity\Apotti findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Apotti patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Apotti[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Apotti|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Apotti saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Apotti[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Apotti[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Apotti[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Apotti[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ApottisTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('apottis');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('apotti_no')
            ->maxLength('apotti_no', 10)
            ->requirePresence('apotti_no', 'create')
            ->notEmptyString('apotti_no');

        $validator
            ->scalar('apotti_type')
            ->maxLength('apotti_type', 100)
            ->requirePresence('apotti_type', 'create')
            ->notEmptyString('apotti_type');

        $validator
            ->dateTime('apotti_date')
            ->requirePresence('apotti_date', 'create')
            ->notEmptyDateTime('apotti_date');

        $validator
            ->scalar('apotti_kari_organization')
            ->maxLength('apotti_kari_organization', 255)
            ->requirePresence('apotti_kari_organization', 'create')
            ->notEmptyString('apotti_kari_organization');

        $validator
            ->scalar('apotti_krito_office')
            ->maxLength('apotti_krito_office', 255)
            ->requirePresence('apotti_krito_office', 'create')
            ->notEmptyString('apotti_krito_office');

        $validator
            ->scalar('apotti_year')
            ->requirePresence('apotti_year', 'create')
            ->notEmptyString('apotti_year');

        $validator
            ->scalar('apotti_duration')
            ->maxLength('apotti_duration', 255)
            ->requirePresence('apotti_duration', 'create')
            ->notEmptyString('apotti_duration');

        $validator
            ->scalar('apotti_title')
            ->requirePresence('apotti_title', 'create')
            ->notEmptyString('apotti_title');

        $validator
            ->scalar('apotti_description')
            ->requirePresence('apotti_description', 'create')
            ->notEmptyString('apotti_description');

        $validator
            ->scalar('audit_organization_reply')
            ->requirePresence('audit_organization_reply', 'create')
            ->notEmptyString('audit_organization_reply');

        $validator
            ->scalar('auditee_comment')
            ->requirePresence('auditee_comment', 'create')
            ->notEmptyString('auditee_comment');

        $validator
            ->scalar('auditor_recommendation')
            ->requirePresence('auditor_recommendation', 'create')
            ->notEmptyString('auditor_recommendation');

        $validator
            ->scalar('apotti_attachment')
            ->requirePresence('apotti_attachment', 'create')
            ->notEmptyString('apotti_attachment');

        return $validator;
    }
}
