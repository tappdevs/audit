<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Apotti Entity
 *
 * @property int $id
 * @property string $apotti_no
 * @property string $apotti_type
 * @property \Cake\I18n\FrozenTime $apotti_date
 * @property string $apotti_kari_organization
 * @property string $apotti_krito_office
 * @property string $apotti_year
 * @property string $apotti_duration
 * @property string $apotti_title
 * @property string $apotti_description
 * @property string $audit_organization_reply
 * @property string $auditee_comment
 * @property string $auditor_recommendation
 * @property string $apotti_attachment
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Apotti extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'apotti_no' => true,
        'apotti_type' => true,
        'apotti_date' => true,
        'apotti_ministry' => true,
        'apotti_kari_organization' => true,
        'apotti_krito_office' => true,
        'apotti_year' => true,
        'apotti_duration' => true,
        'apotti_title' => true,
        'apotti_description' => true,
        'audit_organization_reply' => true,
        'auditee_comment' => true,
        'auditor_recommendation' => true,
        'apotti_attachment' => true,
        'apotti_attachment_other' => true,
        'created' => true,
        'modified' => true,
    ];
}
