<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApottiAttachment Entity
 *
 * @property int $id
 * @property int $apotti_id
 * @property string $attachment_type
 * @property string $attachment_name
 * @property string $attachment_path
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 *
 * @property \App\Model\Entity\Apotti $apotti
 */
class ApottiAttachment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'apotti_id' => true,
        'attachment_type' => true,
        'attachment_name' => true,
        'attachment_path' => true,
        'created' => true,
        'updated' => true,
        'apotti' => true,
    ];
}
