<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApottiTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApottiTable Test Case
 */
class ApottiTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ApottiTable
     */
    protected $Apotti;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Apotti',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Apotti') ? [] : ['className' => ApottiTable::class];
        $this->Apotti = TableRegistry::getTableLocator()->get('Apotti', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Apotti);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
