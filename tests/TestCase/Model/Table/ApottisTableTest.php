<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApottisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApottisTable Test Case
 */
class ApottisTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ApottisTable
     */
    protected $Apottis;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Apottis',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Apottis') ? [] : ['className' => ApottisTable::class];
        $this->Apottis = TableRegistry::getTableLocator()->get('Apottis', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Apottis);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
