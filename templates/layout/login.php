<!DOCTYPE html>
<html class="h-100">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeTitle ? $cakeTitle : 'CakePHP' ?> | অডিট এপ
        <?php #echo $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css([
        'bootstrap.min',
        'fontawesome/css/all.min',
        'style.css'
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?php echo $this->Html->scriptBlock(sprintf('var csrfToken = %s;', json_encode($this->request->getAttribute('csrfToken')))); ?>
</head>
<body class="d-flex flex-column h-100 overflow-hidden">

<div class="login-container flex-fill d-flex justify-content-center align-items-center">
    <div class="shadow-lg border border-secondary p-5  rounded" style="min-width: 420px">
        <div class="text-center mb-3">
            <a href="<?= $this->Url->Build('/');?>"><img src="<?= $this->Url->image('logo.png'); ?>" alt="logo" height="50"></a>
        </div>
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
</div>
<?= $this->Html->script([
    'jquery-3.4.1.slim.min',
    'popper.min',
    'bootstrap.bundle.min',

    'app-jquery',
]) ?>
</body>
</html>
