<!DOCTYPE html>
<html class="h-100">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeTitle ? $cakeTitle : 'CakePHP' ?> | অডিট এপ
        <?php #echo $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css([
        'bootstrap.min',
        'fontawesome/css/all.min',
        'perfect-scrollbar',
        'daterangepicker.min',
        'select2.min',
        'quill.snow',
        'jquery.fancybox.min',
        'style.css'
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?php echo $this->Html->scriptBlock(sprintf('var csrfToken = %s;', json_encode($this->request->getAttribute('csrfToken')))); ?>
</head>
<body class="d-flex flex-column h-100 overflow-hidden">


<header class="border-bottom shadow-sm p-2 d-flex justify-content-between">
    <div class="header-left d-flex align-items-end">
        <div class="logo">
            <a href="<?= $this->Url->build('/apotti'); ?>"><img src="<?= $this->Url->image('logo.png'); ?>" width="150"
                                                                alt="logo"></a>
        </div>
        <button class="btn bg-transparent rounded-0 ml-2 sidebar-left-toggler"><i class="fas fa-bars"></i></button>
    </div>
    <div class="header-right d-flex justify-content-between align-items-center flex-fill">
        <div>
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link<?= isset($param) && $param  == 'Apotti/officeConfiguration' ? ' active' : ''; ?>"
                       href="<?= $this->Url->build(["controller" => "Apotti", "action" => "officeConfiguration"]); ?>">অফিস
                        কনফিগারেশন</a>
                </li>
            </ul>
        </div>
        <div class="d-flex align-items-center">

            <div class="dropdown">
                <button data-toggle="dropdown" class="btn bg-transparent outline-0" id="notification"><i
                        class="far fa-bell"></i></button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="notification">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </div>
            <button class="btn bg-transparent rounded-0 sidebar-right-toggler"><i class="fas fa-bars"></i></button>

            <div class="dropdown ml-3">
                <img src="<?= $this->Url->image('profile.jpg'); ?>" alt="profile" class="img-thumbnail dropdown-toggle"
                     width="36"
                     data-toggle="dropdown" id="dropdownMenuButton">
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">একাউন্ট</a>
                    <a class="dropdown-item" href="#">ইউজার প্রোফাইল</a>
                    <a class="dropdown-item" href="<?= $this->Url->build(["controller" => "Users", "action" => "logout"]); ?>">সাইন আউট</a>
                </div>
            </div>
            <div class="ml-3"><?= $authUser;?></div>
        </div>
    </div>
</header>
<main class="flex-fill overflow-hidden">
    <div class="d-flex h-100">
        <div class="sidebar-left shadow-sm show">
            <?= $this->element('left-sidebar'); ?>
        </div>
        <div class="d-flex flex-column flex-fill overflow-hidden">
            <div class="main-container flex-fill p-5 pScroll">
                <?= $this->Flash->render() ?>
                <?= $this->fetch('content') ?>
            </div>

            <footer class="px-3 py-2 border-top d-flex justify-content-between shadow-sm">
                <div class="copyright-text">
                    কপিরাইট &copy; অডিট বিভাগ ২০২০
                </div>
                <div class="ml-auto d-flex">
                    <span>পার্টনার: </span>
                    <div class="partners d-flex">
                        <a href="#" title="bangladehs"><img alt="bangladehs"
                                                            src="<?= $this->Url->image('ban-gov_logo.png'); ?>"></a>
                        <a href="#" title="a2i"><img alt="a2i" src="<?= $this->Url->image('a_a2i-logo.jpg'); ?>"></a>
                        <a href="#" title="BCC"><img alt="BCC" src="<?= $this->Url->image('bcc_logo.png'); ?>"></a>
                        <a href="#" title="DOICT"><img alt="DOICT"
                                                       src="<?= $this->Url->image('doict_logo.jpg'); ?>"></a>
                        <a href="#" title="TAPPWARE"><img alt="tappware"
                                                          src="<?= $this->Url->image('tappware_logo.png'); ?>"></a>
                    </div>
                </div>
            </footer>
        </div>
        <div class="sidebar-right shadow-sm">
            <?= $this->element('right-sidebar'); ?>
        </div>
    </div>


</main>
<?= $this->Html->script([
    'jquery-3.4.1.slim.min',
    'popper.min',
    'bootstrap.bundle.min',
    'perfect-scrollbar.min',
    'quill.min',
    'moment.min',
    'daterangepicker.min',
    'select2.min',
    'jquery.fancybox.min',
    'amcharts/core',
    'amcharts/charts',
    'amcharts/themes/material',
    'amcharts/themes/animated',
    'amcharts/plugins/forceDirected',
    'amcharts/plugins/venn',

    'app-jquery',
    'app-script',
]) ?>
<?= $this->Html->script([
    'amcharts-initial']); ?>
</body>
</html>
