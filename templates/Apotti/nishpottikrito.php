<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Apotti[]|\Cake\Collection\CollectionInterface $apotties
 * @var \App\Model\Entity\Apotti[]|\Cake\Collection\CollectionInterface $apotti_kari_organization
 * @var \App\Model\Entity\Apotti[]|\Cake\Collection\CollectionInterface $apotti_type
 * @var \App\Model\Entity\Apotti[]|\Cake\Collection\CollectionInterface $apotti_ministry
 */
?>

<div class="apottis index content">
    <div class="mb-4 app-border-bottom border-bottom pb-0 d-flex justify-content-between align-items-end">
        <h4><?= __('নিষ্পত্তিকৃত আপত্তিসমূহ ') ?></h4>
        <div class="d-flex">
            <div
                class="btn-group mb--1"><?= $this->Html->link(__('<i class="fas fa-file-excel mr-2"></i> এক্সপোর্ট করুন'), ['action' => 'add'], ['class' => 'btn btn-outline-info d-flex align-items-center', 'escape' => false]) ?>
                <?= $this->Html->link(__('<i class="fas fa-folder-plus mr-2"></i> আপত্তি আপলোড করুন'), ['action' => 'add'], ['class' => 'btn btn-outline-success d-flex align-items-center', 'escape' => false]) ?>
            </div>
        </div>
    </div>

    <div class="apotti_search mb-4">
        <?php echo $this->Form->create(null, ['method' => 'post']); ?>
        <div class="d-flex justify-content-center">
            <div class="input-group border-left w-auto">
                <?= $this->Form->control('apotti_no', ['class' => 'form-control border-left-0', 'label' => false, 'placeholder' => 'আপত্তি নম্বর', 'autocomplete' => 'off']); ?>
                <?= $this->Form->control('apotti_type', ['class' => 'custom-select js-example-basic-single', 'label' => false, 'options' => $apotti_type, 'empty' => '- আপত্তির ধরন -']); ?>
                <?= $this->Form->control('apotti_ministry', ['class' => 'custom-select js-example-basic-single', 'label' => false, 'options' => $apotti_ministry, 'empty' => '- মন্ত্রনালয় -']); ?>
                <?= $this->Form->control('apotti_year', ['class' => 'form-control border-left-0', 'label' => false, 'placeholder' => 'নিরীক্ষা সাল', 'autocomplete' => 'off']); ?>
                <div class="input-group-append">
                    <button class="btn btn-info" type="submit" id="button-addon2"><i class="fa fa-search"></i> আপত্তি
                        খুঁজুন
                    </button>
                    <!--<button class="btn btn-danger" title="রিসেট করুন" type="reset" id="button-addon2"><i class="fas fa-sync-alt"></i> রিসেট করুন</button>-->
                </div>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>

    <?php if (!empty($apotties->toArray())): ?>

        <div class="table-responsive">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="sorting text-truncate">
                        ক্রম<?php #echo $this->Paginator->sort('id', '<span>আইডি</span> <i class="fas fa-sort"></i>', ['escape' => false]);?></th>
                    <th class="sorting text-truncate"><?= $this->Paginator->sort('apotti_no', '<span>আপত্তি নম্বর</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]); ?></th>
                    <th class="sorting text-truncate"><?= $this->Paginator->sort('apotti_title', '<span>আপত্তির শিরোনাম</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]) ?></th>
                    <th class="sorting text-truncate"><?= $this->Paginator->sort('apotti_type', '<span>আপত্তির ধরন</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]); ?></th>
                    <th class="sorting text-truncate"><?= $this->Paginator->sort('apotti_krito_office', '<span>নিরীক্ষাধীন অফিস </span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]); ?></th>
                    <th class="sorting text-truncate"><?= $this->Paginator->sort('apotti_year', '<span>নিরীক্ষা সাল</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]); ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($apotties as $key => $apotti): ?>
                    <tr>
                        <td class="text-center"><?php echo $key + 1; ?><?php #echo $this->Number->format($apotti->id) ?></td>
                        <td><?= h($apotti->apotti_no) ?></td>
                        <td><?= h($apotti->apotti_title) ?></td>
                        <td class="text-center"><?= h($apotti->apotti_type) ?></td>
                        <td><?= h($apotti->apotti_krito_office) ?></td>
                        <td class="text-center"><?= h($apotti->apotti_year) ?></td>
                        <td class="actions">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <?= $this->Html->link('<i class="fas fa-link"></i>', ['action' => 'view', $apotti->id], ['class' => 'btn btn-sm btn-success', 'escape' => false]) ?>
                                <?= $this->Html->link('<i class="fas fa-edit"></i>', ['action' => 'edit', $apotti->id], ['class' => 'btn btn-sm btn-info', 'escape' => false]) ?>
                                <?= $this->Form->postLink(__('<i class="fas fa-trash"></i>'), ['action' => 'delete', $apotti->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apotti->id), 'class' => 'btn btn-sm btn-danger', 'escape' => false]) ?>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('সর্বপ্রথম')) ?>
                <?= $this->Paginator->prev('<i class="fas fa-angle-left"></i> ' . __(' পূর্ববর্তী '), ['escape' => false]) ?>
                &nbsp;
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__(' পরবর্তী ') . ' <i class="fas fa-angle-right"></i>', ['escape' => false]) ?>
                <?= $this->Paginator->last(__('সর্বশেষ') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
        </div>
    <?php else: ?>
        <div class="p-5 d-flex flex-column align-items-center">
            <h4>কোন আপত্তি পাওয়া যায়নি!</h4>
            <p>নতুন আপত্তি তৈরী করতে চাইলে ক্লিক করুন</p>
            <?= $this->Html->link(__('<i class="fas fa-2x fa-folder-plus mr-2"></i> নতুন আপত্তি'), ['action' => 'add'], ['class' => 'button d-flex align-items-center', 'escape' => false]) ?>
        </div>
    <?php endif; ?>

</div>
