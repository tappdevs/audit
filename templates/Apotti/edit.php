<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Apotti $apotties
 */
?>
<h4 class="mb-4 app-border-bottom border-bottom pb-3">আপত্তি সম্পাদন: <?= $apotties->apotti_no; ?></h4>
<?php echo $this->Form->create($apotties, ['type' => 'file']); ?>
<?= $this->Form->control('attachment_delete', ['type' => 'hidden', 'value' => $this->Url->build(['controller' => 'ApottiAttachments', 'action' => 'delete'])]); ?>

<div class="form-row">
    <div class="form-group col-lg-1">
        <?= $this->Form->control('apotti_no', ['class' => 'form-control', 'label' => 'আপত্তি নম্বর']); ?>
    </div>
    <div class="form-group col-lg">
        <?= $this->Form->control('apotti_type', ['class' => 'custom-select', 'label' => 'আপত্তির ধরন', 'options' => ['জালিয়াতী' => 'জালিয়াতী', 'আর্থিক ক্ষতি' => 'আর্থিক ক্ষতি', 'আর্থিক বিধির ব্যত্যয়' => 'আর্থিক বিধির ব্যত্যয়', 'নিরীক্ষাকালীন অসহযোগীতা' => 'নিরীক্ষাকালীন অসহযোগীতা', 'অন্যান্য' => 'অন্যান্য'],'empty'=>'- আপত্তির ধরন বাছাই করুন -']); ?>
    </div>
    <div class="form-group col-lg-2">
        <?= $this->Form->control('apotti_date', ['class' => 'form-control daterange-date', 'label' => 'আপত্তির তারিখ', 'type' => 'text']); ?>
    </div>
    <div class="form-group col-lg">
        <?= $this->Form->control('apotti_kari_organization', ['class' => 'form-control', 'label' => 'আপত্তিকারী প্রতিষ্ঠান']); ?>
    </div>
</div>
<?= $this->Form->control('apotti_ministry', ['hidden', 'label' => false]); ?>
<div class="form-row">
    <div class="form-group col-lg">
        <?= $this->Form->control('apotti_krito_office', [
            'class' => 'form-control',
            'escape' => false,
            'label' => '<span>নিরীক্ষাধীন অফিস</span> <button type="button" class="fas btn bg-transparent p-0 fa-external-link-square-alt ml-5" data-toggle="modal" data-target="#exampleModal"></button>']);
        ?>
    </div>
    <div class="form-group col-lg-1">
        <?= $this->Form->control('apotti_year', ['class' => 'form-control', 'label' => 'নিরীক্ষা সাল', 'type' => 'number', 'value' => date('Y')]); ?>
    </div>
    <div class="form-group col-lg-3">
        <?= $this->Form->control('apotti_duration', ['class' => 'form-control daterange', 'label' => 'আপত্তির ব্যাপ্তিকাল']); ?>
    </div>
</div>
<div class="form-group">
    <?= $this->Form->control('apotti_title', ['class' => 'form-control', 'label' => 'আপত্তির শিরোনাম', 'placeholder' => 'শিরোনাম লিখুন', 'type' => 'text']); ?>
</div>
<div class="form-group">
    <?= $this->Form->control('apotti_description', ['class' => 'form-control', 'label' => 'আপত্তির বর্ননা', 'hidden']); ?>
    <div class="apotti_description editQuill" id="apotti_description"><?= $apotties->apotti_description; ?></div>
</div>

<div class="form-group">

    <?= $this->Form->control('audit_organization_reply', ['class' => 'form-control', 'label' => 'অডিট প্রতিষ্ঠানের জবাব', 'hidden']); ?>
    <div class="audit_organization_reply editQuill"
         id="audit_organization_reply"><?= $apotties->audit_organization_reply; ?></div>

</div>
<div class="form-group">
    <?= $this->Form->control('auditee_comment', ['class' => 'form-control', 'label' => 'নিরীক্ষা মন্তব্য', 'hidden']); ?>
    <div class="auditee_comment editQuill" id="auditee_comment"><?= $apotties->auditee_comment; ?></div>
</div>
<div class="form-group">
    <?= $this->Form->control('auditor_recommendation', ['class' => 'form-control', 'label' => 'নিরীক্ষা সুপারিশ', 'hidden']); ?>
    <div class="auditor_recommendation editQuill"
         id="auditor_recommendation"><?= $apotties->auditor_recommendation; ?></div>
</div>


<div class="form-group">
    <label for="inputEmail4">আপত্তির মূল সংযুক্তি</label>
    <div class="form-row">
        <div class="input-group mb-3 col-lg-4">
            <div class="custom-file">
                <?= $this->Form->control('apotti_attachment[]', ['type' => 'file', 'multiple', 'label' => '', 'class' => 'custom-file-input']); ?>
                <label class="custom-file-label" for="inputGroupFile01">ফাইল বাছাই করুন</label>
            </div>
        </div>
    </div>
    <div class="w-50 preview"></div>
    <div class="form-row attachment-loader flex-wrap">
        <?php
        foreach ($attachedFilesMain as $attachment) {
            $pathinfo = pathinfo($attachment['attachment_path']);
            $fileExtension = $pathinfo['extension'];
            $attachment_url = str_replace('\\', '/', $attachment['attachment_path']);
            $attachment_user_define_name = $attachment['user_define_name'];
            ?>
            <div class="attached-item border d-flex flex-column mb-3 mx-1">
                <div class="attach-loader w-100 h-100 position-absolute">
                    <div
                        class="d-flex align-items-center justify-content-center h-100 w-100">
                        <div class="loader"></div>
                    </div>
                </div>
                <button type="button" data-attach="<?= $attachment['id']; ?>"
                        class="btn btn-danger btn-sm d-flex align-items-end close-btn"><i
                        class="fas fa-times"></i></button>

                <?php if (in_array($fileExtension, array("jpg", "png", "jpeg"))) { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                       href="<?= $this->Url->image($attachment_url); ?>" data-fancybox="gallery">
                        <img src="<?= $this->Url->image($attachment_url); ?>"
                             alt="<?= $attachment_user_define_name; ?>">
                    </a>
                <?php } elseif ($fileExtension == 'docx') { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center" target="_blank"
                       href="<?= $this->Url->image($attachment_url); ?>">
                        <i class="fas fa-file-word fa-3x"></i>
                    </a>
                <?php } elseif ($fileExtension == 'pdf') { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center" target="_blank"
                       href="<?= $this->Url->image($attachment_url); ?>">
                        <i class="fas fa-file-pdf fa-3x"></i>
                    </a>
                <?php } else { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center" target="_blank"
                       href="<?= $this->Url->image($attachment_url); ?>">
                        <i class="fas fa-file fa-3x"></i>
                    </a>
                <?php } ?>

                <p class="text-truncate mb-0 px-3 py-2"><?= $attachment_user_define_name; ?></p>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<div class="form-group">
    <label for="inputEmail4">আপত্তির অন্যান্ন সংযুক্তি</label>
    <div class="form-row">
        <div class="input-group mb-3 col-lg-4">
            <div class="custom-file">
                <?= $this->Form->control('apotti_attachment_other[]', ['type' => 'file', 'multiple', 'label' => '', 'class' => 'custom-file-input']); ?>
                <label class="custom-file-label" for="inputGroupFile01">ফাইল বাছাই করুন</label>
            </div>
        </div>
    </div>
    <div class="w-50 preview"></div>
    <div class="form-row attachment-loader flex-wrap">
        <?php
        foreach ($attachedFilesOther as $attachment) {
            $pathinfo = pathinfo($attachment['attachment_path']);
            $fileExtension = $pathinfo['extension'];
            $attachment_url = str_replace('\\', '/', $attachment['attachment_path']);
            $attachment_user_define_name = $attachment['user_define_name'];
            ?>
            <div class="attached-item border d-flex flex-column mb-3 mx-1">
                <div class="attach-loader w-100 h-100 position-absolute">
                    <div
                        class="d-flex align-items-center justify-content-center h-100 w-100">
                        <div class="loader"></div>
                    </div>
                </div>
                <button type="button" data-attach="<?= $attachment['id']; ?>"
                        class="btn btn-danger btn-sm d-flex align-items-end close-btn"><i
                        class="fas fa-times"></i></button>

                <?php if (in_array($fileExtension, array("jpg", "png", "jpeg"))) { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                       href="<?= $this->Url->image($attachment_url); ?>" data-fancybox="gallery">
                        <img src="<?= $this->Url->image($attachment_url); ?>"
                             alt="<?= $attachment_user_define_name; ?>">
                    </a>
                <?php } elseif ($fileExtension == 'docx') { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center" target="_blank"
                       href="<?= $this->Url->image($attachment_url); ?>">
                        <i class="fas fa-file-word fa-3x"></i>
                    </a>
                <?php } elseif ($fileExtension == 'pdf') { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center" target="_blank"
                       href="<?= $this->Url->image($attachment_url); ?>">
                        <i class="fas fa-file-pdf fa-3x"></i>
                    </a>
                <?php } else { ?>
                    <a class="flex-fill border-bottom d-flex align-items-center justify-content-center" target="_blank"
                       href="<?= $this->Url->image($attachment_url); ?>">
                        <i class="fas fa-file fa-3x"></i>
                    </a>
                <?php } ?>

                <p class="text-truncate mb-0 px-3 py-2"><?= $attachment_user_define_name; ?></p>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<?= $this->Form->button('<i class="far fa-save mr-2"></i> সংরক্ষণ করুন', [
    'type' => 'submit',
    'escapeTitle' => false,
    'name' => 'btn-save',
    'value' => 'save',
    'class' => 'btn btn-outline-success btn-lg'
]); ?>

<?= $this->Form->button('<i class="far fa-save mr-2"></i> সংরক্ষণ করে অডিট তালিকায় যান', [
    'type' => 'submit',
    'escapeTitle' => false,
    'name' => 'btn-save',
    'value' => 'save-list',
    'class' => 'btn btn-outline-info btn-lg'
]); ?>
<?php echo $this->Form->end(); ?>
<?= $this->element('apotti_krito_office'); ?>
