<h4 class="mb-4 app-border-bottom border-bottom pb-3">নতুন আপত্তি</h4>
<?php echo $this->Form->create($apotti, ['type' => 'file']); ?>

<div class="form-row">
    <div class="form-group col-lg-1">
        <?= $this->Form->control('apotti_no', ['class' => 'form-control', 'label' => 'আপত্তি নম্বর']); ?>
    </div>
    <div class="form-group col-lg">
        <?= $this->Form->control('apotti_type', ['class' => 'custom-select', 'label' => 'আপত্তির ধরন', 'options' => ['one', 'two']]); ?>
    </div>
    <div class="form-group col-lg-2">
        <?= $this->Form->control('apotti_date', ['class' => 'form-control daterange-date en_to_bn', 'label' => 'আপত্তির তারিখ', 'type' => 'text']); ?>
    </div>
    <div class="form-group col-lg">
        <?= $this->Form->control('apotti_kari_organization', ['class' => 'form-control', 'label' => 'আপত্তিকারী প্রতিষ্ঠান']); ?>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-lg">
        <?= $this->Form->control('apotti_krito_office', [
            'class' => 'form-control',
            'escape' => false,
            'label' => '<span>আপত্তিকৃত অফিস</span> <button type="button" class="fas btn bg-transparent p-0 fa-external-link-square-alt ml-5" data-toggle="modal" data-target="#exampleModal"></button>']);
        ?>
    </div>
    <div class="form-group col-lg-1">
        <?= $this->Form->control('apotti_year', ['class' => 'form-control', 'label' => 'আপত্তি বর্ষ', 'type' => 'number', 'value' => date('Y')]); ?>
    </div>
    <div class="form-group col-lg-3">
        <?= $this->Form->control('apotti_duration', ['class' => 'form-control daterange', 'label' => 'আপত্তির ব্যাপ্তিকাল']); ?>
    </div>
</div>
<div class="form-group">
    <?= $this->Form->control('apotti_title', ['class' => 'form-control', 'label' => 'আপত্তির শিরোনাম', 'placeholder' => 'শিরোনাম লিখুন', 'type' => 'text']); ?>
</div>
<div class="form-group">
    <?= $this->Form->control('apotti_description', ['class' => 'form-control', 'label' => 'আপত্তির বর্ননা', 'hidden']); ?>
    <div class="apotti_description editQuill" id="apotti_description"></div>
</div>

<div class="form-group">

    <?= $this->Form->control('audit_organization_reply', ['class' => 'form-control', 'label' => 'অডিট প্রতিষ্ঠানের জবাব', 'hidden']); ?>
    <div class="audit_organization_reply editQuill" id="audit_organization_reply"></div>

</div>
<div class="form-group">
    <?= $this->Form->control('auditee_comment', ['class' => 'form-control', 'label' => 'নিরীক্ষা মন্তব্য', 'hidden']); ?>
    <div class="auditee_comment editQuill" id="auditee_comment"></div>
</div>
<div class="form-group">
    <?= $this->Form->control('auditor_recommendation', ['class' => 'form-control', 'label' => 'নিরীক্ষা সুপারিশ', 'hidden']); ?>
    <div class="auditor_recommendation editQuill" id="auditor_recommendation"></div>
</div>


<div class="form-group">
    <label for="inputEmail4">মূল আপত্তি সংযুক্তি</label>
    <div class="form-row">
        <div class="input-group mb-3 col-lg-4">
            <div class="custom-file">
                <?= $this->Form->control('apotti_attachment[]', ['type' => 'file', 'multiple', 'label' => '', 'class' => 'custom-file-input']); ?>
                <label class="custom-file-label" for="inputGroupFile01">ফাইল বাছাই করুন</label>
            </div>
        </div>
    </div>
    <div class="w-50 preview"></div>
</div>

<div class="form-group">
    <label for="inputEmail4">অন্যান্ন আপত্তির সংযুক্তি</label>
    <div class="form-row">
        <div class="input-group mb-3 col-lg-4">
            <div class="custom-file">
                <?= $this->Form->control('apotti_attachment[]', ['type' => 'file', 'multiple', 'label' => '', 'class' => 'custom-file-input']); ?>
                <label class="custom-file-label" for="inputGroupFile01">ফাইল বাছাই করুন</label>
            </div>
        </div>
    </div>
    <div class="w-50 preview"></div>
</div>
<!--div class="form-group">
    <div class="form-check">
        <input class="form-check-input" type="checkbox" id="gridCheck">
        <label class="form-check-label" for="gridCheck">
            এপ্রুভের জন্য পাঠান
        </label>
    </div>
</div-->
<?= $this->Form->button('<i class="far fa-save mr-2"></i> সংরক্ষণ করুন', [
    'type' => 'submit',
    'escapeTitle' => false,
    'class' => 'btn btn-outline-success btn-lg'
]); ?>
<?php echo $this->Form->end(); ?>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">আপত্তিকৃত অফিস</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body apotti_krito_office">
                <div class="form-row">
                    <div class="col-lg">
                        <select class="custom-select">
                            <option>- মন্ত্রনালয় / ডিরেক্টরেট -</option>
                            <option selected value="শিক্ষা মন্ত্রণালয়">শিক্ষা মন্ত্রণালয়</option>
                            <option value="এলজিইডি">এলজিইডি</option>
                            <option value="অর্থ মন্ত্রণালয়">অর্থ মন্ত্রণালয়</option>
                            <option value="রাষ্ট্রপতির কার্যালয়">রাষ্ট্রপতির কার্যালয়</option>
                            <option value="কৃষি মন্ত্রণালয়">কৃষি মন্ত্রণালয়</option>
                            <option value="প্রতিরক্ষা মন্ত্রণালয়">প্রতিরক্ষা মন্ত্রণালয়</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select class="custom-select">
                            <option>- বিভাগ -</option>
                            <option value="অর্থ বিভাগ">অর্থ বিভাগ</option>
                            <option selected value="অর্থনৈতিক সম্পর্ক বিভাগ">অর্থনৈতিক সম্পর্ক বিভাগ</option>
                            <option value="আর্থিক প্রতিষ্ঠান বিভাগ">আর্থিক প্রতিষ্ঠান বিভাগ</option>
                            <option value="অভ্যন্তরীণ সম্পদ বিভাগ">অভ্যন্তরীণ সম্পদ বিভাগ</option>
                            <option value="মাধ্যমিক ও উচ্চ শিক্ষা বিভাগ">মাধ্যমিক ও উচ্চ শিক্ষা বিভাগ</option>
                            <option value="কারিগরি ও মাদ্রাসা শিক্ষা বিভাগ">কারিগরি ও মাদ্রাসা শিক্ষা বিভাগ</option>
                        </select>
                    </div>
                    <div class="col-lg">
                        <select class="custom-select">
                            <option>- প্রজেক্ট / ইনষ্টিটিউট -</option>
                            <option value="বাংলাদেশ হাউস বিল্ডিং ফাইন্যান্স কর্পোরেশন">বাংলাদেশ হাউস বিল্ডিং ফাইন্যান্স
                                কর্পোরেশন
                            </option>
                            <option selected value="ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ">ইনভেস্টমেন্ট কর্পোরেশন অব
                                বাংলাদেশ
                            </option>
                            <option value="অফিস অফ দি কন্ট্রোলার জেনারেল অফ একাউন্টস">অফিস অফ দি কন্ট্রোলার জেনারেল অফ
                                একাউন্টস
                            </option>
                            <option value="বাংলাদেশ কম্পট্রোলার এন্ড অডিটর জেনারেল">বাংলাদেশ কম্পট্রোলার এন্ড অডিটর
                                জেনারেল
                            </option>
                            <option value="প্রজেক্ট">প্রজেক্ট</option>
                            <option value="ইনষ্টিটিউট">ইনষ্টিটিউট</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between align-items-start">
                <div class="alert col alert-warning py-1 px-2 apotti_krito_office_text">'আমার বাড়ি আমার খামার' প্রকল্প,
                    পল্লী উন্নয়ন ও সমবায় বিভাগ
                </div>
                <div class="flex-nowrap">
                    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i
                            class="far fa-times-circle mr-2"></i>বন্ধ করুন
                    </button>
                    <button type="button" class="btn btn-sm btn-outline-success apotti_krito_office_add"
                            data-dismiss="modal"><i
                            class="fas fa-plus-circle mr-2"></i> অফিস যোগ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
