<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Apotti $apotti
 */

function formatedText($input)
{
    return preg_replace('/<p[^>]*><br><\\/p[^>]*>/', '', html_entity_decode($input));
}

?>
<h4 class="mb-4 app-border-bottom border-bottom pb-3"><?= __('আপত্তি নম্বর: ') ?> <?= h($apotti->apotti_no) ?></h4>
<div class="toolbar d-flex justify-content-between">
    <div class="toolbar-left d-flex">
        <div class="btn-group" role="group" aria-label="Basic example">
            <?= $this->Html->link(__('<i class="fas fa-long-arrow-alt-left mr-1"></i> পিছনে যান'), ['action' => 'index'], ['class' => 'btn btn-light shadow-sm', 'escape' => false]) ?>


            <button class="btn btn-light shadow-sm mx-2" type="button" data-toggle="tooltip" title="star"
                    data-original-title="বিশেষ">
                <span class="far fa-star"></span>
            </button>

            <div class="shadow-sm btn-group">
                <?= $this->Html->link(__('<i class="far fa-edit"></i>'), ['action' => 'edit', $apotti->id], ['class' => 'btn btn-light', 'escape' => false]) ?>
                <?= $this->Form->postLink(__('<i class="far fa-trash-alt"></i>'), ['action' => 'delete', $apotti->id], ['class' => 'btn btn-light', 'escape' => false, 'confirm' => __('Are you sure you want to delete # {0}?', $apotti->id)]) ?>
                <?= $this->Html->link(__('<i class="fas fa-list"></i>'), ['action' => 'index'], ['class' => 'btn btn-light', 'escape' => false]) ?>
                <?= $this->Html->link(__('<i class="fas fa-folder-plus"></i>'), ['action' => 'add'], ['class' => 'btn btn-light', 'escape' => false]) ?>
            </div>
            <?= $this->Html->link(__('<i class="fas fa-print mr-1"></i> প্রিন্ট প্রিভিউ'), ['action' => 'add'], ['class' => 'btn btn-light shadow-sm ml-2 apotti_preview_btn', 'type' => 'button', 'escape' => false]) ?>
        </div>
    </div>
    <div class="d-flex align-items-center">
        <span class="mr-2">১, সর্বমোট: ২৩,৪২৩</span>
        <div class="btn-group shadow-sm">
            <button class="btn btn-light" type="button">
                <span class="fa fa-chevron-left"></span>
            </button>
            <button class="btn btn-light" type="button">
                <span class="fa fa-chevron-right"></span>
            </button>
        </div>
    </div>
</div>
<div class="apotti_preview">
    <div class="py-3">
        <div class="block-title my-3 fs-20">
            <span class="font-weight-semibold">আপত্তির শিরোনাম: </span> <?= h($apotti->apotti_title); ?>
        </div>

        <div class="decision pt-2 d-flex justify-content-between">
            <div>
            <span class="badge badge-pill badge-light border font-weight-normal mr-1 shadow-sm">আপত্তির ধরন: <span
                    class="en_to_bn_text"><?= h($apotti->apotti_type) ?></span></span>
                <span class="badge badge-pill badge-light border font-weight-normal mr-1 shadow-sm">নীরিক্ষা সাল: <span
                        class="en_to_bn_text"><?= h($apotti->apotti_year) ?></span></span>
                <span class="badge badge-pill badge-light border font-weight-normal mr-1 shadow-sm">আপত্তির তারিখ: <span
                        class="en_to_bn_text"><?= date('d-m-Y', strtotime($apotti->apotti_date)); ?></span></span>
            </div>

        </div>
    </div>

    <div class="header justify-content-between mt-0 border-top border-bottom py-2">
        <div class="d-flex justify-content-between">
            <div>
                <div class="fs-16 font-weight-normal">
                <span
                    class="font-weight-semibold">আপত্তিকারী প্রতিষ্ঠান:</span> <?= h($apotti->apotti_kari_organization) ?>
                </div>
                <div class="daak_sender">
                    <span class="font-weight-semibold">নীরিক্ষাধীন অফিস:</span> <?= h($apotti->apotti_krito_office) ?>
                </div>
            </div>
            <div class="text-right d-flex flex-column">
                <div class="fs-12 d-flex justify-content-end">
                    <div class="mr-2">
                        <svg class="fill-violate align-middle mr-1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
                             width="14px"
                             viewBox="0 0 484.5 484.5" style="enable-background:new 0 0 484.5 484.5;"
                             xml:space="preserve"><path
                                d="M372.3,84.15c-7.649-12.75-22.95-20.4-40.8-20.4H51c-28.05,0-51,22.95-51,51v255c0,28.05,22.95,51,51,51h280.5    c17.85,0,33.15-7.65,40.8-20.4l112.2-158.1L372.3,84.15z"></path></svg>
                        সর্বোচ্চ অগ্রাধিকার
                    </div>
                    <div>
                        <svg class="fill-yellow align-middle mr-1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px"
                             width="14px"
                             viewBox="0 0 484.5 484.5" style="enable-background:new 0 0 484.5 484.5;"
                             xml:space="preserve"><path
                                d="M372.3,84.15c-7.649-12.75-22.95-20.4-40.8-20.4H51c-28.05,0-51,22.95-51,51v255c0,28.05,22.95,51,51,51h280.5    c17.85,0,33.15-7.65,40.8-20.4l112.2-158.1L372.3,84.15z"></path></svg>
                        বিশেষ গোপনীয়
                    </div>
                </div>
                <div class="d-flex">
                    <div class="date fs-12 ml-3"><span
                            class="font-weight-semibold">আপত্তির ব্যাপ্তিকাল:</span> <?= h($apotti->apotti_duration) ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="py-3">
        <div class="apotti view content">

            <div class="text mb-5">
                <h5 class="mb-4 app-border-bottom border-bottom pb-3"><?= __('আপত্তির বিবরণ') ?></h5>
                <blockquote>
                    <?= formatedText($this->Text->autoParagraph(h($apotti->apotti_description))); ?>
                </blockquote>
            </div>
            <div class="text mb-5">
                <h5 class="mb-4 app-border-bottom border-bottom pb-3"><?= __('অডিট প্রতিষ্ঠানের জবাব') ?></h5>
                <blockquote>
                    <?= formatedText($this->Text->autoParagraph(h($apotti->audit_organization_reply))); ?>
                </blockquote>
            </div>
            <div class="text">
                <h5 class="mb-4 app-border-bottom border-bottom pb-3"><?= __('নিরীক্ষা মন্তব্য') ?></h5>
                <blockquote>
                    <?= formatedText($this->Text->autoParagraph(h($apotti->auditee_comment))); ?>
                </blockquote>
            </div>
            <div class="text">
                <h5 class="mb-4 app-border-bottom border-bottom pb-3"><?= __('নিরীক্ষার সুপারিশ') ?></h5>
                <blockquote>
                    <?= formatedText($this->Text->autoParagraph(h($apotti->auditor_recommendation))); ?>
                </blockquote>
            </div>
            <div class="text">
                <h5 class="mb-4 app-border-bottom border-bottom pb-3"><?= __('আপত্তির মূল সংযুক্তি') ?></h5>
                <div class="form-row attachment-loader flex-wrap">
                    <?php
                    foreach ($attachedFilesMain as $attachment) {
                        $pathinfo = pathinfo($attachment['attachment_path']);
                        $fileExtension = $pathinfo['extension'];
                        $attachment_url = str_replace('\\', '/', $attachment['attachment_path']);
                        $attachment_user_define_name = $attachment['user_define_name'];
                        ?>
                        <div class="attached-item border d-flex flex-column mb-3 mx-1">
                            <?php if (in_array($fileExtension, array("jpg", "png", "jpeg"))) { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   href="<?= $this->Url->image($attachment_url); ?>" data-fancybox="gallery">
                                    <img src="<?= $this->Url->image($attachment_url); ?>"
                                         alt="<?= $attachment_user_define_name; ?>">
                                </a>
                            <?php } elseif ($fileExtension == 'docx') { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   target="_blank" href="<?= $this->Url->image($attachment_url); ?>">
                                    <i class="fas fa-file-word fa-3x"></i>
                                </a>
                            <?php } elseif ($fileExtension == 'pdf') { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   target="_blank" href="<?= $this->Url->image($attachment_url); ?>">
                                    <i class="fas fa-file-pdf fa-3x"></i>
                                </a>
                            <?php } else { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   target="_blank" href="<?= $this->Url->image($attachment_url); ?>">
                                    <i class="fas fa-file fa-3x"></i>
                                </a>
                            <?php } ?>

                            <p class="text-truncate mb-0 px-3 py-2"><?= $attachment_user_define_name; ?></p>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>

            <div class="text">
                <h5 class="mb-4 app-border-bottom border-bottom pb-3"><?= __('আপত্তির অন্যান্য সংযুক্তি') ?></h5>
                <div class="form-row attachment-loader flex-wrap">
                    <?php
                    foreach ($attachedFilesOther as $attachment) {
                        $pathinfo = pathinfo($attachment['attachment_path']);
                        $fileExtension = $pathinfo['extension'];
                        $attachment_url = str_replace('\\', '/', $attachment['attachment_path']);
                        $attachment_user_define_name = $attachment['user_define_name'];
                        ?>
                        <div class="attached-item border d-flex flex-column mb-3 mx-1">
                            <?php if (in_array($fileExtension, array("jpg", "png", "jpeg"))) { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   href="<?= $this->Url->image($attachment_url); ?>" data-fancybox="gallery">
                                    <img src="<?= $this->Url->image($attachment_url); ?>"
                                         alt="<?= $attachment_user_define_name; ?>">
                                </a>
                            <?php } elseif ($fileExtension == 'docx') { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   target="_blank" href="<?= $this->Url->image($attachment_url); ?>">
                                    <i class="fas fa-file-word fa-3x"></i>
                                </a>
                            <?php } elseif ($fileExtension == 'pdf') { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   target="_blank" href="<?= $this->Url->image($attachment_url); ?>">
                                    <i class="fas fa-file-pdf fa-3x"></i>
                                </a>
                            <?php } else { ?>
                                <a class="flex-fill border-bottom d-flex align-items-center justify-content-center"
                                   target="_blank" href="<?= $this->Url->image($attachment_url); ?>">
                                    <i class="fas fa-file fa-3x"></i>
                                </a>
                            <?php } ?>

                            <p class="text-truncate mb-0 px-3 py-2"><?= $attachment_user_define_name; ?></p>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
