<div class="pScroll">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link<?= isset($param) && $param=='Dashboard/index' ? ' active' : ''; ?>" href="<?= $this->Url->build(["controller" => "Dashboard", "action" => "index"]); ?>">
                <i class="fas fa-tachometer-alt mr-2"></i>
                <span>ড্যাশবোন্ড</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link<?= isset($param) && $param=='Apotti/index' ? ' active' : ''; ?>"
               href="<?= $this->Url->build(["controller" => "Apotti", "action" => "index"]); ?>">
                <i class="fas fa-folder-open mr-2"></i>
                <span>আপত্তিসমূহ</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link<?= isset($param) && $param  == 'Apotti/add' ? ' active' : ''; ?>"
               href="<?= $this->Url->build(["controller" => "Apotti", "action" => "add"]); ?>">
                <i class="fas fa-folder-plus mr-2"></i>
                <span>আপত্তি আপলোড করুন</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link<?= isset($param) && $param  == 'NishpottikritoApotti/index' ? ' active' : ''; ?>" href="<?= $this->Url->build(["controller" => "Apotti", "action" => "nishpottikrito"]); ?>">
                <i class="fas fa-pencil-ruler mr-2"></i>
                <span>নিষ্পত্তিকৃত আপত্তি</span>
            </a>
        </li>
    </ul>
</div>
