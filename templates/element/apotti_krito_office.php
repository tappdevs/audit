<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> নিরীক্ষাধীন অফিস </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body apotti_krito_office">
                <div class="form-row">
                    <div class="col-lg">
                        <select class="custom-select" id="apotti_ministry">
                            <option>- মন্ত্রনালয় / ডিরেক্টরেট -</option>
                            <option selected value="শিক্ষা মন্ত্রণালয়">শিক্ষা মন্ত্রণালয়</option>
                            <option value="এলজিইডি">এলজিইডি</option>
                            <option value="অর্থ মন্ত্রণালয়">অর্থ মন্ত্রণালয়</option>
                            <option value="রাষ্ট্রপতির কার্যালয়">রাষ্ট্রপতির কার্যালয়</option>
                            <option value="কৃষি মন্ত্রণালয়">কৃষি মন্ত্রণালয়</option>
                            <option value="প্রতিরক্ষা মন্ত্রণালয়">প্রতিরক্ষা মন্ত্রণালয়</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select class="custom-select" id="apotti_section">
                            <option>- বিভাগ -</option>
                            <option value="অর্থ বিভাগ">অর্থ বিভাগ</option>
                            <option selected value="অর্থনৈতিক সম্পর্ক বিভাগ">অর্থনৈতিক সম্পর্ক বিভাগ</option>
                            <option value="আর্থিক প্রতিষ্ঠান বিভাগ">আর্থিক প্রতিষ্ঠান বিভাগ</option>
                            <option value="অভ্যন্তরীণ সম্পদ বিভাগ">অভ্যন্তরীণ সম্পদ বিভাগ</option>
                            <option value="মাধ্যমিক ও উচ্চ শিক্ষা বিভাগ">মাধ্যমিক ও উচ্চ শিক্ষা বিভাগ</option>
                            <option value="কারিগরি ও মাদ্রাসা শিক্ষা বিভাগ">কারিগরি ও মাদ্রাসা শিক্ষা বিভাগ</option>
                        </select>
                    </div>
                    <div class="col-lg">
                        <select class="custom-select" id="apotti_institute">
                            <option>- প্রজেক্ট / ইনষ্টিটিউট -</option>
                            <option value="বাংলাদেশ হাউস বিল্ডিং ফাইন্যান্স কর্পোরেশন">বাংলাদেশ হাউস বিল্ডিং ফাইন্যান্স
                                কর্পোরেশন
                            </option>
                            <option selected value="ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ">ইনভেস্টমেন্ট কর্পোরেশন অব
                                বাংলাদেশ
                            </option>
                            <option value="অফিস অফ দি কন্ট্রোলার জেনারেল অফ একাউন্টস">অফিস অফ দি কন্ট্রোলার জেনারেল অফ
                                একাউন্টস
                            </option>
                            <option value="বাংলাদেশ কম্পট্রোলার এন্ড অডিটর জেনারেল">বাংলাদেশ কম্পট্রোলার এন্ড অডিটর
                                জেনারেল
                            </option>
                            <option value="প্রজেক্ট">প্রজেক্ট</option>
                            <option value="ইনষ্টিটিউট">ইনষ্টিটিউট</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between align-items-start">
                <div class="alert col alert-warning py-1 px-2 apotti_krito_office_text">'আমার বাড়ি আমার খামার' প্রকল্প,
                    পল্লী উন্নয়ন ও সমবায় বিভাগ
                </div>
                <div class="flex-nowrap">
                    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i
                            class="far fa-times-circle mr-2"></i>বন্ধ করুন
                    </button>
                    <button type="button" class="btn btn-sm btn-outline-success apotti_krito_office_add"
                            data-dismiss="modal"><i
                            class="fas fa-plus-circle mr-2"></i> অফিস যোগ করুন
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
