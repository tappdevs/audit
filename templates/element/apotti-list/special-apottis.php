<?php if (!empty($apotties->toArray())): ?>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th class="sorting text-truncate"><?= $this->Paginator->sort('apotti_no', '<span>আপত্তি নম্বর</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]); ?></th>
                <th class="sorting text-truncate text-left"><?= $this->Paginator->sort('apotti_title', '<span>আপত্তির শিরোনাম</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]) ?></th>
                <th class="sorting text-truncate text-left"><?= $this->Paginator->sort('apotti_type', '<span>আপত্তির ধরন</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]); ?></th>
                <th class="sorting text-truncate"><?= $this->Paginator->sort('apotti_year', '<span>নিরীক্ষা সাল</span> <i class="fas fa-sort ml-3"></i>', ['escape' => false]); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($apotties as $key => $apotti): ?>
                <tr>
                    <td><?= h($apotti->apotti_no) ?></td>
                    <td class="text-left"><?= h($apotti->apotti_title) ?></td>
                    <td class="text-left"><?= h($apotti->apotti_type) ?></td>
                    <td><?= h($apotti->apotti_year) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
