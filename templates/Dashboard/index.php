<div class="mb-4 app-border-bottom border-bottom">
    <h4>ড্যাশবোর্ড</h4>
</div>
<div class="row flex-wrap">

    <div class="mb-5 col-lg-6">
        <div class="bg-light p-3 shadow-sm text-center">
            <div class="mb-4 app-border-bottom border-bottom border-center"><h5>সর্বমোট আপত্তি</h5></div>
            <div id="chartdivCombinedBullet" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
    <div class="mb-5 col-lg-6">
        <div class="bg-light p-3 shadow-sm text-center">
            <div class="mb-4 app-border-bottom border-bottom border-center"><h5>আপত্তি ধরনের তথ্য</h5></div>
            <div id="chartdivVariableHeight" style="width: 100%; height: 400px;"></div>
        </div>
    </div>
</div>
<div class="bg-light p-3 shadow-sm text-center">
    <div class="mb-4 app-border-bottom border-bottom d-flex justify-content-between">
        <h5>আপত্তির সামারি</h5>
        <ul class="nav nav-pills" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                   aria-controls="home"
                   aria-selected="true">সর্বশেষ আপত্তিসমূহ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                   aria-controls="profile" aria-selected="false">বিশেষ আপত্তিসমূহ</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab"
                   aria-controls="contact" aria-selected="false">নিষ্পন্নকৃত আপত্তিসমূহ</a>
            </li>
        </ul>
    </div>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <?= $this->element('apotti-list/latest-apottis'); ?>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <?= $this->element('apotti-list/special-apottis'); ?>
        </div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <?= $this->element('apotti-list/completed-apottis'); ?>
        </div>
    </div>
</div>
