<!-- in /templates/Users/login.php -->
<div class="users form">
    <?= $this->Flash->render() ?>
    <div class="text-center">
        <h3>ইউজার লগ-ইন</h3>
        <p>প্রবেশের জন্য আপনার ই-মেইল ও পাসওয়ার্ড দিন</p>
    </div>
    <?= $this->Form->create() ?>
    <div
        class="mb-3"><?= $this->Form->control('email', ['required' => true, 'class' => 'form-control shadow-sm', 'label' => false, 'placeholder' => 'আপনার ইমেইল লিখুন']) ?></div>
    <div
        class="mb-3"><?= $this->Form->control('password', ['required' => true, 'class' => 'form-control shadow-sm', 'label' => false, 'placeholder' => 'পাসওয়ার্ড লিখুন']) ?></div>
    <div
        class="mb-3"><?= $this->Form->submit(__('লগইন করুন'), ['class' => 'btn btn-primary shadow-sm btn-lg btn-block']); ?></div>

    <?= $this->Form->end() ?>

    <?/*= $this->Html->link("নতুন ইউজার", ['action' => 'add'], ['class' => 'btn btn-warning shadow-sm btn-lg btn-block']) */?>
</div>

