<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

<div class="users form content">
    <div class="text-center">
        <h3>Add User </h3>
    </div>
    <?= $this->Form->create($user) ?>
    <div class="mb-3">
        <?= $this->Form->control('email', ['class' => 'form-control shadow-sm', 'label' => false, 'placeholder' => 'type your email...']); ?>
    </div>
    <div class="mb-3">
        <?= $this->Form->control('password', ['class' => 'form-control shadow-sm', 'label' => false, 'placeholder' => 'type your password...']); ?>
    </div>

    <?= $this->Form->button(__('Add User'), ['class' => 'btn btn-primary shadow-sm btn-lg btn-block']) ?>
    <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'btn btn-warning shadow-sm btn-lg btn-block']) ?>
    <?= $this->Form->end() ?>
</div>

