<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ApottiAttachment $apottiAttachment
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $apottiAttachment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $apottiAttachment->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Apotti Attachments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="apottiAttachments form content">
            <?= $this->Form->create($apottiAttachment) ?>
            <fieldset>
                <legend><?= __('Edit Apotti Attachment') ?></legend>
                <?php
                    echo $this->Form->control('apotti_id', ['options' => $apottis]);
                    echo $this->Form->control('attachment_type');
                    echo $this->Form->control('attachment_name');
                    echo $this->Form->control('attachment_path');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
