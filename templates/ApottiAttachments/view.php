<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ApottiAttachment $apottiAttachment
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Apotti Attachment'), ['action' => 'edit', $apottiAttachment->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Apotti Attachment'), ['action' => 'delete', $apottiAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apottiAttachment->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Apotti Attachments'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Apotti Attachment'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="apottiAttachments view content">
            <h3><?= h($apottiAttachment->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Apotti') ?></th>
                    <td><?= $apottiAttachment->has('apotti') ? $this->Html->link($apottiAttachment->apotti->id, ['controller' => 'Apottis', 'action' => 'view', $apottiAttachment->apotti->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Attachment Type') ?></th>
                    <td><?= h($apottiAttachment->attachment_type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Attachment Name') ?></th>
                    <td><?= h($apottiAttachment->attachment_name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Attachment Path') ?></th>
                    <td><?= h($apottiAttachment->attachment_path) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($apottiAttachment->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($apottiAttachment->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Updated') ?></th>
                    <td><?= h($apottiAttachment->updated) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
