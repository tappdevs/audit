<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ApottiAttachment[]|\Cake\Collection\CollectionInterface $apottiAttachments
 */
?>
<div class="apottiAttachments index content">
    <?= $this->Html->link(__('New Apotti Attachment'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Apotti Attachments') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('apotti_id') ?></th>
                    <th><?= $this->Paginator->sort('attachment_type') ?></th>
                    <th><?= $this->Paginator->sort('attachment_name') ?></th>
                    <th><?= $this->Paginator->sort('attachment_path') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('updated') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($apottiAttachments as $apottiAttachment): ?>
                <tr>
                    <td><?= $this->Number->format($apottiAttachment->id) ?></td>
                    <td><?= $apottiAttachment->has('apotti') ? $this->Html->link($apottiAttachment->apotti->id, ['controller' => 'Apottis', 'action' => 'view', $apottiAttachment->apotti->id]) : '' ?></td>
                    <td><?= h($apottiAttachment->attachment_type) ?></td>
                    <td><?= h($apottiAttachment->attachment_name) ?></td>
                    <td><?= h($apottiAttachment->attachment_path) ?></td>
                    <td><?= h($apottiAttachment->created) ?></td>
                    <td><?= h($apottiAttachment->updated) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $apottiAttachment->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $apottiAttachment->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $apottiAttachment->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apottiAttachment->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
