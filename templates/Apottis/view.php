<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Apotti $apotti
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Apotti'), ['action' => 'edit', $apotti->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Apotti'), ['action' => 'delete', $apotti->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apotti->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Apottis'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Apotti'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="apottis view content">
            <h3><?= h($apotti->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Apotti No') ?></th>
                    <td><?= h($apotti->apotti_no) ?></td>
                </tr>
                <tr>
                    <th><?= __('Apotti Type') ?></th>
                    <td><?= h($apotti->apotti_type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Apotti Kari Organization') ?></th>
                    <td><?= h($apotti->apotti_kari_organization) ?></td>
                </tr>
                <tr>
                    <th><?= __('Apotti Krito Office') ?></th>
                    <td><?= h($apotti->apotti_krito_office) ?></td>
                </tr>
                <tr>
                    <th><?= __('Apotti Year') ?></th>
                    <td><?= h($apotti->apotti_year) ?></td>
                </tr>
                <tr>
                    <th><?= __('Apotti Duration') ?></th>
                    <td><?= h($apotti->apotti_duration) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($apotti->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Apotti Date') ?></th>
                    <td><?= h($apotti->apotti_date) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($apotti->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($apotti->modified) ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Apotti Title') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($apotti->apotti_title)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Apotti Description') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($apotti->apotti_description)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Audit Organization Reply') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($apotti->audit_organization_reply)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Auditee Comment') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($apotti->auditee_comment)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Auditor Recommendation') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($apotti->auditor_recommendation)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Apotti Attachment') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($apotti->apotti_attachment)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
