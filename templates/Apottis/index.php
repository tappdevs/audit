<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Apotti[]|\Cake\Collection\CollectionInterface $apottis
 */
?>
<div class="apottis index content">
    <?= $this->Html->link(__('New Apotti'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Apottis') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('apotti_no') ?></th>
                    <th><?= $this->Paginator->sort('apotti_type') ?></th>
                    <th><?= $this->Paginator->sort('apotti_date') ?></th>
                    <th><?= $this->Paginator->sort('apotti_kari_organization') ?></th>
                    <th><?= $this->Paginator->sort('apotti_krito_office') ?></th>
                    <th><?= $this->Paginator->sort('apotti_year') ?></th>
                    <th><?= $this->Paginator->sort('apotti_duration') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($apottis as $apotti): ?>
                <tr>
                    <td><?= $this->Number->format($apotti->id) ?></td>
                    <td><?= h($apotti->apotti_no) ?></td>
                    <td><?= h($apotti->apotti_type) ?></td>
                    <td><?= h($apotti->apotti_date) ?></td>
                    <td><?= h($apotti->apotti_kari_organization) ?></td>
                    <td><?= h($apotti->apotti_krito_office) ?></td>
                    <td><?= h($apotti->apotti_year) ?></td>
                    <td><?= h($apotti->apotti_duration) ?></td>
                    <td><?= h($apotti->created) ?></td>
                    <td><?= h($apotti->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $apotti->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $apotti->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $apotti->id], ['confirm' => __('Are you sure you want to delete # {0}?', $apotti->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
