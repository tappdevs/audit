<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Apotti $apotti
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $apotti->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $apotti->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Apottis'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="apottis form content">
            <?= $this->Form->create($apotti) ?>
            <fieldset>
                <legend><?= __('Edit Apotti') ?></legend>
                <?php
                    echo $this->Form->control('apotti_no');
                    echo $this->Form->control('apotti_type');
                    echo $this->Form->control('apotti_date');
                    echo $this->Form->control('apotti_kari_organization');
                    echo $this->Form->control('apotti_krito_office');
                    echo $this->Form->control('apotti_year');
                    echo $this->Form->control('apotti_duration');
                    echo $this->Form->control('apotti_title');
                    echo $this->Form->control('apotti_description');
                    echo $this->Form->control('audit_organization_reply');
                    echo $this->Form->control('auditee_comment');
                    echo $this->Form->control('auditor_recommendation');
                    echo $this->Form->control('apotti_attachment');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
