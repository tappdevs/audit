(function () {

    var current = location.pathname;

    $('[data-toggle="popover"]').popover()

    $('input.daterange-search').daterangepicker({
        autoUpdateInput: false,
        opens: 'left',
    });
    $('input.daterange').daterangepicker({
        opens: 'left',
    });
    $('input.daterange-date').daterangepicker({
        /*locale: {
            format: 'YYYY-MM-DD',
        },*/
        opens: 'left',
        singleDatePicker: true,
    });


    $(document).off('click', ".fa-close").on('click', ".fa-close", function () {
        $(this).closest('.up_item').remove();
    })

    $('.js-example-basic-single').select2();


    /*window.onload = function() {
        if (window.File && window.FileList && window.FileReader) {
            var filesInput = document.getElementById("uploadImage");
            filesInput.addEventListener("change", function(event) {
                var files = event.target.files;
                var output = document.getElementById("result");
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    if (!file.type.match('image'))
                        continue;
                    var picReader = new FileReader();
                    picReader.addEventListener("load", function(event) {
                        var picFile = event.target;
                        var div = document.createElement("div");
                        div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" +
                            "title='" + picFile.name + "'/>";
                        output.insertBefore(div, null);
                    });
                    picReader.readAsDataURL(file);
                }

            });
        }
    }*/


})(jQuery)
