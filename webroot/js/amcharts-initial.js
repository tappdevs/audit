if(document.getElementById('chartdivCombinedBullet')) {
    am4core.ready(function () {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chart = am4core.create("chartdivCombinedBullet", am4charts.XYChart);

// Add data
        chart.data = [{
            "apottiDate": "2004",
            "totalApotti": 500,
            "doneApotti": 800
        }, {
            "apottiDate": "2005",
            "totalApotti": 400,
            "doneApotti": 600
        }, {
            "apottiDate": "2006",
            "totalApotti": 500,
            "doneApotti": 200
        }, {
            "apottiDate": "2007",
            "totalApotti": 800,
            "doneApotti": 900
        }, {
            "apottiDate": "2008",
            "totalApotti": 900,
            "doneApotti": 600
        }, {
            "apottiDate": "2008",
            "totalApotti": 300,
            "doneApotti": 500
        }, {
            "apottiDate": "2009",
            "totalApotti": 500,
            "doneApotti": 700
        }, {
            "apottiDate": "2010",
            "totalApotti": 700,
            "doneApotti": 600
        }, {
            "apottiDate": "2011",
            "totalApotti": 900,
            "doneApotti": 500
        }, {
            "apottiDate": "2012",
            "totalApotti": 500,
            "doneApotti": 800
        }, {
            "apottiDate": "2013",
            "totalApotti": 400,
            "doneApotti": 800
        }, {
            "apottiDate": "2014",
            "totalApotti": 300,
            "doneApotti": 400
        }, {
            "apottiDate": "2015",
            "totalApotti": 500,
            "doneApotti": 700
        }, {
            "apottiDate": "2016",
            "totalApotti": 500,
            "doneApotti": 800
        }, {
            "apottiDate": "2017",
            "totalApotti": 400,
            "doneApotti": 700
        }];

// Create axes
        var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
//dateAxis.renderer.grid.template.location = 0;
        dateAxis.renderer.minGridDistance = 30;

        var valueTotalApotti = chart.yAxes.push(new am4charts.ValueAxis());
        valueTotalApotti.title.text = "আপত্তি সংখ্যা";


// Create series
        var totalApottiSeries = chart.series.push(new am4charts.ColumnSeries());
        totalApottiSeries.dataFields.valueY = "totalApotti";
        totalApottiSeries.dataFields.dateX = "apottiDate";
        totalApottiSeries.yAxis = valueTotalApotti;
        totalApottiSeries.name = "সম্পন্নকৃত আপত্তিসমূহ";
        totalApottiSeries.tooltipText = `{name}: {valueY}টি[/]`;
        totalApottiSeries.fill = chart.colors.getIndex(0);
        totalApottiSeries.strokeWidth = 0;
        totalApottiSeries.clustered = false;
        totalApottiSeries.columns.template.width = am4core.percent(40);

        var doneApottiSeries = chart.series.push(new am4charts.ColumnSeries());
        doneApottiSeries.dataFields.valueY = "doneApotti";
        doneApottiSeries.dataFields.dateX = "apottiDate";
        doneApottiSeries.yAxis = valueTotalApotti;
        doneApottiSeries.name = "মোট আপত্তি সংখ্যা";
        doneApottiSeries.tooltipText = `{name}: {valueY}টি[/]`;
        doneApottiSeries.fill = chart.colors.getIndex(0).lighten(0.5);
        doneApottiSeries.strokeWidth = 0;
        doneApottiSeries.clustered = false;
        doneApottiSeries.toBack();


// Add cursor
        chart.cursor = new am4charts.XYCursor();

// Add legend
        chart.legend = new am4charts.Legend();
        chart.legend.position = "top";

// Add scrollbar
        chart.scrollbarX = new am4charts.XYChartScrollbar();
        chart.scrollbarX.series.push(totalApottiSeries);
        chart.scrollbarX.parent = chart.bottomAxesContainer;

    }); // end am4core.ready()
}

/*=========================================================================*/

if(document.getElementById('chartdivVariableHeight')) {

    am4core.ready(function () {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

        var chart = am4core.create("chartdivVariableHeight", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.data = [
            {
                country: "জালিয়াতী ",
                litres: 501.9
            },
            {
                country: "আর্থিক ক্ষতি",
                litres: 301.9
            },
            {
                country: "আর্থিক বিধির ব্যত্যয় ",
                litres: 201.1
            },
            {
                country: "নীরিক্ষাকালীন অসহযোগীতা ",
                litres: 165.8
            },
            {
                country: "অন্যান্য",
                litres: 465.8
            }
        ];

        chart.innerRadius = am4core.percent(40);
        chart.depth = 120;

        chart.legend = new am4charts.Legend();

        var series = chart.series.push(new am4charts.PieSeries3D());
        series.dataFields.value = "litres";
        series.dataFields.depthValue = "litres";
        series.dataFields.category = "country";
        series.slices.template.cornerRadius = 5;
        series.colors.step = 3;

    });
}
