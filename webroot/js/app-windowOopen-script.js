const apottiPreviewBtn = document.querySelector('.apotti_preview_btn')
if (apottiPreviewBtn) {
    apottiPreviewBtn.onclick = (e) => {
        e.preventDefault();

        var apottiPreviewWindow = window.open(' ', "apottiPreviewWindow", "width=1100,height=800,scrollbars=yes");
        const headStyles = document.getElementsByTagName('link');
        for (i = 0; i < headStyles.length; i++) {
            apottiPreviewWindow.document.write('<link rel="stylesheet" type="text/css" href="' + headStyles[i].href + '" />');
        }
        const apottiPreviewElement = document.querySelector('.apotti_preview');

        apottiPreviewWindow.document.write('<div id="apottiPreviewer" class="p-5">' + apottiPreviewElement.outerHTML + '</div>')


        var apottiPreviewer = document.querySelector('#apottiPreviewer');
        apottiPreviewWindow.document.write('')
        console.log(apottiPreviewer.innerHTML)

        apottiPreviewWindow.document.write(apottiPreviewWindow)
    }
}


const apottiMinistry = document.getElementById('apotti-ministry');

if (apottiMinistry) {
    document.getElementById('apotti-ministry').value = document.getElementById('apotti_ministry').value;

    document.getElementById('apotti_ministry').onchange = () => {
        document.getElementById('apotti-ministry').value = document.getElementById('apotti_ministry').value;
    }
}
/*attachment delete*/
const closeBtn = document.querySelectorAll('.close-btn')
const attachmentDeleteUrl = document.getElementById('attachment-delete');

for (i = 0; i < closeBtn.length; i++) {
    closeBtn[i].onclick = function (e) {
        var attachID = this.getAttribute('data-attach');
        console.log(attachID)

        if (confirm("Are you sure to delete the attachment?")) {
            e.target.closest('.attached-item').classList.add('loading')
            var requestUrl = attachmentDeleteUrl.value + '/' + attachID;
            console.log(requestUrl)
            let response = fetch(requestUrl, {
                method: 'POST',
                body: '',
                headers: {
                    'X-CSRF-Token': csrfToken
                }
            })
                .then(function (response) {
                    return response.text();
                })
                .then(function (text) {
                    e.target.closest('.attached-item').remove();
                    console.log('Request successful');
                })
                .catch(function (error) {
                    console.log('Request failed', error)
                });

        }
    }
}
/*attachment delete*/


const sidebarLeft = document.querySelector('.sidebar-left')
const sidebarRight = document.querySelector('.sidebar-right')
const sidebarLeftToggler = document.querySelector('.sidebar-left-toggler')
const sidebarRightToggler = document.querySelector('.sidebar-right-toggler')

sidebarLeftToggler.onclick = () => sidebarLeft.classList.toggle("show");
sidebarRightToggler.onclick = () => sidebarRight.classList.toggle("show");

var pScroll = document.querySelectorAll('.pScroll')
for (i = 0; i < pScroll.length; i++) {
    const ps = new PerfectScrollbar(pScroll[i], {
        wheelSpeed: 2,
        wheelPropagation: true,
        minScrollbarLength: 20
    });
}


function editorData(elementQuell) {
    var editorAr = [];
    var quill = new Quill(elementQuell, {
        modules: {
            toolbar: true
        },
        theme: 'snow'
    });
    quill.on('text-change', function () {
        elementQuell.parentNode.querySelector('textarea').innerHTML = quill.container.firstChild.innerHTML;
    });
}


const allQuell = document.querySelectorAll('.apotti_description, .audit_organization_reply, .auditee_comment, .auditor_recommendation');
for (i = 0, qLength = allQuell.length; i < qLength; i++) editorData(document.querySelector('#' + allQuell[i].id))


/*apotti_krito_office*/
var officeView = document.querySelector('.apotti_krito_office_text');
var apottiKritoOffice = document.querySelectorAll('.apotti_krito_office select');

function apottiLoadView(apottiKritoOffice) {
    if (apottiKritoOffice.length) {
        var apottiKritoOfficeArray = [];
        for (var i = 0; i < apottiKritoOffice.length; i++) apottiKritoOfficeArray.push(apottiKritoOffice[i].value)


        officeView.innerHTML = apottiKritoOfficeArray.reverse().join(', ');
    }
}

apottiLoadView(apottiKritoOffice)
for (var i = 0; i < apottiKritoOffice.length; i++) {
    apottiKritoOffice[i].addEventListener('change', (event) => {
        apottiLoadView(apottiKritoOffice)
    });
    apottiKritoOfficeArray = [];
}
if (document.querySelector('.apotti_krito_office_add')) {
    document.querySelector('.apotti_krito_office_add').onclick = (ev) => {
        document.querySelector('#apotti-krito-office').value = document.querySelector('.apotti_krito_office_text').textContent
    }
}

/*apotti_krito_office*/

function selectText(node) {
    node = document.querySelector(node);

    if (document.body.createTextRange) {
        const range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        const selection = window.getSelection();
        const range = document.createRange();
        range.selectNodeContents(node);
        selection.removeAllRanges();
        selection.addRange(range);
    } else {
        console.warn("Could not select text in node: Unsupported browser.");
    }
}

const clickable = document.querySelector('.apotti_krito_office_text');
if (clickable) {
    clickable.addEventListener('click', () => selectText('.apotti_krito_office_text'));
}

/*number en to bn*/
var numbers = {0: '০', 1: '১', 2: '২', 3: '৩', 4: '৪', 5: '৫', 6: '৬', 7: '৭', 8: '৮', 9: '৯'};

function replaceNumbers(inputElement) {
    for (var i = 0, output = [], inputVal = inputElement.value; i < inputVal.length; ++i)
        numbers.hasOwnProperty(inputVal[i]) ? output.push(numbers[inputVal[i]]) : output.push(inputVal[i])
    inputElement.value = output.join('')
}

function replaceNumbersText(inputElement) {
    for (var i = 0, output = [], inputVal = inputElement.textContent; i < inputVal.length; ++i)
        numbers.hasOwnProperty(inputVal[i]) ? output.push(numbers[inputVal[i]]) : output.push(inputVal[i])
    inputElement.textContent = output.join('')
}

var enToBnInput = document.querySelectorAll('.en_to_bn');
for (i = 0; i < enToBnInput.length; i++) {
    replaceNumbers(enToBnInput[i]);
    enToBnInput[i].onchange = function () {
        replaceNumbers(this);
    };
}
var enToBnText = document.querySelectorAll('.en_to_bn_text');
for (i = 0; i < enToBnText.length; i++) {
    replaceNumbersText(enToBnText[i]);
}


/*number en to bn*/

/*file_attachment*/
function getFileExtension(filename) {
    return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
}

var attachfiles = document.querySelectorAll('input[type=file]');

for (i = 0; i < attachfiles.length; i++) {
    attachfiles[i].onchange = (attachEvent) => {

        var preview = attachEvent.target.closest('.form-row').nextElementSibling;
        var files = attachEvent.target.files;
        preview.innerHTML = ''

        for (i = 0; i < files.length; i++) {
            var fileExtension = getFileExtension(files[i].name)

            switch (fileExtension) {
                case 'jpg':
                    fileIcon = 'fas fa-file-image';
                    break;
                case 'xlsx':
                case  'xls':
                case  'csv':
                    fileIcon = 'fas fa-file-excel';
                    break;
                case 'doc':
                case 'docx':
                    fileIcon = 'fas fa-file-word';
                    break;
                case 'pdf':
                    fileIcon = 'fas fa-file-pdf';
                    break;
                default:
                    fileIcon = 'fas fa-file-alt';
            }

            var fileViewer = `
            <div class="input-group iamge_preview mb-3">
              <div class="d-flex align-items-center justify-content-center input-group-prepend border border-right-0 px-2"><i class="fa-15x ` + fileIcon + `"></i></div>
              <input type="text" name="user_define_name[]" class="form-control" placeholder="Recipient's username" value="` + files[i].name + `">
              <button onclick="attachmentRemove(this)" data-index="` + i + `" type="button" class="fas fa-trash btn-danger btn removeLink"></button>
            </div>
            `;

            preview.insertAdjacentHTML("beforeend", fileViewer);

        }
    }
}

function attachmentRemove(element) {
    console.log(element)
    element.parentNode.remove()
    console.log(element.getAttribute('data-index'))
}


/*document.querySelector('.removeLink').addEventListener("click", function (e) {
    console.log(e.target.getAttribute('data-index'))
})*/


/* document.querySelector('.removeLink').onclick = (e) => {

     console.log(e.target.getAttribute('data-index'))
     // var removeID =

     /!*for (i = 0; i < files.length; ++i) {
         if (files[i].name == fileName) {
             //console.log("match at: " + i);
             // remove the one element at the index where we get a match
             files.splice(i, 1);
         }
     }*!/
 }*/


/*function readAndPreview(file) {

    // Make sure `file.name` matches our extensions criteria
    // if ( /\.(doc?x|doc|xlxs|pdf)$/i.test(file.name) ) {
    if (/\.(doc?x)$/i.test(file.name)) {
        // console.log(file)
        // preview.appendChild('<i class="fas fa-file-word"></i>');
        // preview.innerHTML = '<i class="fas fa-file-word"></i>';
    }
    if (/\.(jpe?g|png|gif)$/i.test(file.name)) {
        // console.log(file)
        // preview.innerHTML = '<i class="fas fa-file-image"></i>';
    }

}

if (files) {
    [].forEach.call(files, readAndPreview);
}*/

/*file_attachment*/

