-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2020 at 03:59 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cake_audit`
--

-- --------------------------------------------------------

--
-- Table structure for table `apotti`
--

CREATE TABLE `apotti` (
  `id` int(11) NOT NULL,
  `apotti_no` varchar(10) NOT NULL,
  `apotti_type` varchar(100) NOT NULL,
  `apotti_date` date NOT NULL,
  `apotti_ministry` varchar(100) NOT NULL,
  `apotti_kari_organization` varchar(255) NOT NULL,
  `apotti_krito_office` varchar(255) NOT NULL,
  `apotti_year` year(4) NOT NULL,
  `apotti_duration` varchar(255) NOT NULL,
  `apotti_title` text NOT NULL,
  `apotti_description` text NOT NULL,
  `audit_organization_reply` text NOT NULL,
  `auditee_comment` text NOT NULL,
  `auditor_recommendation` text NOT NULL,
  `apotti_attachment` text NOT NULL,
  `apotti_attachment_other` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `apotti`
--

INSERT INTO `apotti` (`id`, `apotti_no`, `apotti_type`, `apotti_date`, `apotti_ministry`, `apotti_kari_organization`, `apotti_krito_office`, `apotti_year`, `apotti_duration`, `apotti_title`, `apotti_description`, `audit_organization_reply`, `auditee_comment`, `auditor_recommendation`, `apotti_attachment`, `apotti_attachment_other`, `created`, `modified`) VALUES
(8, 'AAN001', '1', '2020-04-25', 'শিক্ষা মন্ত্রণালয়', 'বিসমিল্লাহ ট্রেডিং ', 'ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়', 2020, '04/15/2020 - 04/15/2020', 'সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েছেন', '<p>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েছেনছেনছেন</p>', '<p>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েছেন ছেনছেন ছেনছেন ছেনছেনছেনছেনছেন</p>', '<p>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েছেনছেনছেন</p>', '<p>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েসরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েছেনছেনছেনছেনছেন</p>', '', '', '2020-04-15 08:23:10', '2020-04-18 13:02:43'),
(9, 'AAN002', '2', '2020-03-19', 'অর্থ মন্ত্রণালয়', 'বাংলাদেশ ট্রেডিং', 'ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, অর্থ মন্ত্রণালয়', 2020, '04/15/2020 - 04/15/2020', 'আজ রোববার ৫ এপ্রিল পর্যন্ত সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েছেন', '<ol><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li></ol>', '<ol><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li></ol><p>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </p><p><br></p>', '<ul><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li></ul><p><br></p>', '<ul><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li><li>সরকারের রোগতত্ত্ব, রোগনিয়ন্ত্রণ ও গবেষণা প্রতিষ্ঠান (আইইডিসিআর) জানিয়েছে, </li></ul><p><br></p>', '', '', '2020-04-15 08:25:21', '2020-04-15 12:30:24'),
(10, 'AAN003', '1', '2020-04-10', 'এলজিইডি', 'বাংলাদেশ ট্রেডিং', 'কর্পোরেশন অব বাংলাদেশ ইনভেস্টমেন্ট , অর্থনৈতিক সম্পর্ক বিভাগ, এলজিইডি', 2020, '04/15/2020 - 04/15/2020', 'সারা দেশে ৮৮ জন ব্যক্তি করোনা ভাইরাসে আক্রান্ত হয়েছেন', '<p>কর্পোরেশন অব বাংলাদেশ ইনভেস্টমেন্ট , অর্থনৈতিক সম্পর্ক বিভাগ, এলজিইডি</p>', '<p>কর্পোরেশন অব বাংলাদেশ ইনভেস্টমেন্ট , অর্থনৈতিক সম্পর্ক বিভাগ, এলজিইডি</p>', '<p>কর্পোরেশন অব বাংলাদেশ ইনভেস্টমেন্ট , অর্থনৈতিক সম্পর্ক বিভাগ, এলজিইডি</p>', '<p>কর্পোরেশন অব বাংলাদেশ ইনভেস্টমেন্ট , অর্থনৈতিক সম্পর্ক বিভাগ, এলজিইডি</p>', '', '', '2020-04-15 08:40:50', '2020-04-16 15:11:32'),
(12, 'AAN004', '3', '2020-04-25', '', 'ইসলাম ট্রেডিং', 'ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়', 2020, '04/15/2020 - 04/15/2020', 'ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়', '<ul><li>ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়</li><li>ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়</li><li><br></li></ul>', '<p>ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়</p>', '<p>ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়</p>', '<p>ইনভেস্টমেন্ট কর্পোরেশন অব বাংলাদেশ, অর্থনৈতিক সম্পর্ক বিভাগ, শিক্ষা মন্ত্রণালয়</p>', '', '', '2020-04-15 09:34:33', '2020-04-15 09:44:41'),
(17, 'Ut vel do ', '1', '2020-04-18', '', 'Carr Haney Associates', 'Occaecat ipsum ea facilis doloribus praesentium rem et sapiente incididunt et consequatur dolor aut rerum', 2020, '04/17/2020 - 05/26/2020', 'Consectetur ex rerum', '<p>Exercitation enim ve.</p>', '<p>Ex aut lorem dolor e.</p>', '<p>Distinctio. Ut iste .</p>', '<p>Quo enim laudantium.</p>', '', '', '2020-04-18 11:03:29', '2020-04-18 12:46:20'),
(18, 'Voluptatem', '3', '2020-04-18', '', 'Johnson Hubbard Traders', 'Aperiam et unde saepe ex veritatis eos qui quisquam', 2020, '04/30/2020 - 05/22/2020', 'Dolore esse accusant', '<p>Voluptas irure iste .</p>', '<p>Enim ad consectetur .</p>', '<p>Nulla dignissimos es.</p>', '<p>Molestiae consequat.</p>', '', '', '2020-04-18 11:04:20', '2020-04-18 12:46:35'),
(19, 'Ipsa ea o', '2', '2020-04-18', '', 'Joseph Huber Trading', 'Sit expedita sed omnis quia a quisquam dolorem totam repellendus Voluptate in', 2020, '04/18/2020 - 04/18/2020', 'Aperiam quidem quos ', '<p>Qui aliquip commodo .</p>', '<p>Quam quasi in obcaec.</p>', '<p>Ipsam nihil rerum do.</p>', '<p>Dolorem dicta ut err.</p>', '', '', '2020-04-18 11:04:38', '2020-04-18 12:46:44');

-- --------------------------------------------------------

--
-- Table structure for table `apottis`
--

CREATE TABLE `apottis` (
  `id` int(11) NOT NULL,
  `apotti_no` varchar(10) NOT NULL,
  `apotti_type` varchar(100) NOT NULL,
  `apotti_date` datetime NOT NULL,
  `apotti_kari_organization` varchar(255) NOT NULL,
  `apotti_krito_office` varchar(255) NOT NULL,
  `apotti_year` year(4) NOT NULL,
  `apotti_duration` varchar(255) NOT NULL,
  `apotti_title` text NOT NULL,
  `apotti_description` text NOT NULL,
  `audit_organization_reply` text NOT NULL,
  `auditee_comment` text NOT NULL,
  `auditor_recommendation` text NOT NULL,
  `apotti_attachment` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `apottis`
--

INSERT INTO `apottis` (`id`, `apotti_no`, `apotti_type`, `apotti_date`, `apotti_kari_organization`, `apotti_krito_office`, `apotti_year`, `apotti_duration`, `apotti_title`, `apotti_description`, `audit_organization_reply`, `auditee_comment`, `auditor_recommendation`, `apotti_attachment`, `created`, `modified`) VALUES
(1, 'Voluptatem', 'Nostrud eveniet temporibus incididunt cumque eum enim quae culpa', '0000-00-00 00:00:00', 'Castaneda and Bradley Plc', 'Dolor consectetur qui quaerat veniam beatae enim cum accusamus dolor laudantium anim aperiam aliquip alias', 1990, 'Id nesciunt sint nostrum amet culpa aliqua Explicabo Molestias', 'Eum cupidatat enim s', 'Qui consequuntur inc', 'Aut aliquid molestia', 'Qui sed sunt sed ut ', 'Fugiat itaque quia ', 'Nulla qui perspiciat', '2020-03-29 15:11:04', '2020-03-29 15:11:04'),
(2, '123123', 'Rerum dolore facere doloribus itaque eaque', '0000-00-00 00:00:00', 'Prince and Horn Plc', 'Voluptatum quaerat laboriosam rem blanditiis quos ullamco aut inventore eius id sed', 2009, 'Alias nulla Nam necessitatibus dolorum qui dolor', 'Mollit in reiciendis', 'Qui omnis officia qu', 'Voluptatem facilis d', 'Ea modi velit ipsum', 'Qui obcaecati eos N', 'Elit tempora qui in', '2020-03-30 11:30:15', '2020-03-30 11:30:15');

-- --------------------------------------------------------

--
-- Table structure for table `apotti_attachments`
--

CREATE TABLE `apotti_attachments` (
  `id` int(11) NOT NULL,
  `apotti_id` int(10) UNSIGNED NOT NULL,
  `attachment_type` varchar(100) NOT NULL,
  `user_define_name` varchar(255) NOT NULL,
  `attachment_name` varchar(100) NOT NULL,
  `attachment_path` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `apotti_attachments`
--

INSERT INTO `apotti_attachments` (`id`, `apotti_id`, `attachment_type`, `user_define_name`, `attachment_name`, `attachment_path`, `created`, `updated`) VALUES
(124, 8, 'main', 'apotti image', '8_5e96c46ec3c8a.jpg', 'upload\\apotti_id_8\\', '2020-04-15 08:23:10', '0000-00-00 00:00:00'),
(129, 9, 'main', '5_6269556172211093805.docx', '9_5e96c4f1d3317.docx', 'upload\\apotti_id_9\\', '2020-04-15 08:25:21', '0000-00-00 00:00:00'),
(130, 10, 'main', '1.jpg', '10_5e96c8927ba40.jpg', 'upload\\apotti_id_10\\', '2020-04-15 08:40:50', '0000-00-00 00:00:00'),
(131, 10, 'other', '1e06da7033b383e1d0ae2f96a2ebf69a.jpg', '10_5e96c8928279d.jpg', 'upload\\apotti_id_10\\', '2020-04-15 08:40:50', '0000-00-00 00:00:00'),
(132, 10, 'other', '02bbbcb89ad19b033fe866b75f051486.jpg', '10_5e96c89283589.jpg', 'upload\\apotti_id_10\\', '2020-04-15 08:40:50', '0000-00-00 00:00:00'),
(134, 12, 'main', '1.jpg', '12_5e96d529203fe.jpg', 'upload\\apotti_id_12\\', '2020-04-15 09:34:33', '0000-00-00 00:00:00'),
(135, 12, 'main', '5_6269556172211093805.docx', '12_5e96d52921ff6.docx', 'upload\\apotti_id_12\\', '2020-04-15 09:34:33', '0000-00-00 00:00:00'),
(136, 12, 'other', '5_6269556172211093805.pdf', '12_5e96d52922ec3.pdf', 'upload\\apotti_id_12\\', '2020-04-15 09:34:33', '0000-00-00 00:00:00'),
(138, 9, 'other', 'User define', '9_5e96fe6044134.pdf', 'upload\\apotti_id_9\\', '2020-04-15 12:30:24', '0000-00-00 00:00:00'),
(139, 13, 'main', '2.jpeg', '13_5e9ad14a07512.jpeg', 'upload\\apotti_id_13\\', '2020-04-18 10:07:06', '0000-00-00 00:00:00'),
(140, 14, 'main', '2.jpeg', '14_5e9ad290adda2.jpeg', 'upload\\apotti_id_14\\', '2020-04-18 10:12:32', '0000-00-00 00:00:00'),
(141, 14, 'main', '4. Ward Report.pdf', '14_5e9ad290b4167.pdf', 'upload\\apotti_id_14\\', '2020-04-18 10:12:32', '0000-00-00 00:00:00'),
(142, 14, 'other', '2.jpeg', '14_5e9ad290b577f.jpg', 'upload\\apotti_id_14\\', '2020-04-18 10:12:32', '0000-00-00 00:00:00'),
(143, 14, 'other', '4. Ward Report.pdf', '14_5e9ad290b704d.jpg', 'upload\\apotti_id_14\\', '2020-04-18 10:12:32', '0000-00-00 00:00:00'),
(144, 17, 'main', '1.jpg', '17_5e9ade816b15a.jpg', 'upload\\apotti_id_17\\', '2020-04-18 11:03:29', '0000-00-00 00:00:00'),
(145, 17, 'main', '5_6269556172211093805.docx', '17_5e9ade81713e9.docx', 'upload\\apotti_id_17\\', '2020-04-18 11:03:29', '0000-00-00 00:00:00'),
(146, 17, 'other', '1.jpg', '17_5e9ade8172182.jpg', 'upload\\apotti_id_17\\', '2020-04-18 11:03:29', '0000-00-00 00:00:00'),
(147, 17, 'other', '5_6269556172211093805.docx', '17_5e9ade817385b.pdf', 'upload\\apotti_id_17\\', '2020-04-18 11:03:29', '0000-00-00 00:00:00'),
(148, 17, 'other', '1e06da7033b383e1d0ae2f96a2ebf69a.jpg', '17_5e9ade8174a9f.docx', 'upload\\apotti_id_17\\', '2020-04-18 11:03:29', '0000-00-00 00:00:00'),
(149, 19, 'main', '5_6269556172211093805.pdf', '19_5e9adec652eb9.pdf', 'upload\\apotti_id_19\\', '2020-04-18 11:04:38', '0000-00-00 00:00:00'),
(150, 19, 'other', '5_6269556172211093805.pdf', '19_5e9adec65506f.pdf', 'upload\\apotti_id_19\\', '2020-04-18 11:04:38', '0000-00-00 00:00:00'),
(151, 20, 'main', '5_6269556172211093805.docx', '20_5e9adf092033f.docx', 'upload\\apotti_id_20\\', '2020-04-18 11:05:45', '0000-00-00 00:00:00'),
(152, 20, 'other', '5_6269556172211093805.docx', '20_5e9adf0925cf7.pdf', 'upload\\apotti_id_20\\', '2020-04-18 11:05:45', '0000-00-00 00:00:00'),
(153, 21, 'main', '4. Ward Report.pdf', '21_5e9adf2729428.pdf', 'upload\\apotti_id_21\\', '2020-04-18 11:06:15', '0000-00-00 00:00:00'),
(154, 21, 'other', '4. Ward Report.pdf', '21_5e9adf272b473.docx', 'upload\\apotti_id_21\\', '2020-04-18 11:06:15', '0000-00-00 00:00:00'),
(158, 8, 'other', '1.jpg', '8_5e9af7cad3e63.jpg', 'upload\\apotti_id_8\\', '2020-04-18 12:51:22', '0000-00-00 00:00:00'),
(159, 8, 'other', '1e06da7033b383e1d0ae2f96a2ebf69a.jpg', '8_5e9af7cad596a.jpg', 'upload\\apotti_id_8\\', '2020-04-18 12:51:22', '0000-00-00 00:00:00'),
(160, 8, 'other', '02bbbcb89ad19b033fe866b75f051486.jpg', '8_5e9af7cad6f4e.jpg', 'upload\\apotti_id_8\\', '2020-04-18 12:51:22', '0000-00-00 00:00:00'),
(161, 8, 'other', '2.jpeg', '8_5e9af7cad8337.jpeg', 'upload\\apotti_id_8\\', '2020-04-18 12:51:22', '0000-00-00 00:00:00'),
(162, 8, 'other', '4. Ward Report.pdf', '8_5e9af7cada03b.pdf', 'upload\\apotti_id_8\\', '2020-04-18 12:51:22', '0000-00-00 00:00:00'),
(163, 8, 'other', '5_6269556172211093805.docx', '8_5e9af7cadb2d0.docx', 'upload\\apotti_id_8\\', '2020-04-18 12:51:22', '0000-00-00 00:00:00'),
(164, 8, 'other', '5_6269556172211093805.pdf', '8_5e9af7cadc46e.pdf', 'upload\\apotti_id_8\\', '2020-04-18 12:51:22', '0000-00-00 00:00:00'),
(165, 8, 'other', 'my edit button', '8_5e9af81dab204.pdf', 'upload\\apotti_id_8\\', '2020-04-18 12:52:45', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apotti`
--
ALTER TABLE `apotti`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apottis`
--
ALTER TABLE `apottis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `apotti_attachments`
--
ALTER TABLE `apotti_attachments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apotti`
--
ALTER TABLE `apotti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `apottis`
--
ALTER TABLE `apottis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `apotti_attachments`
--
ALTER TABLE `apotti_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=166;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
